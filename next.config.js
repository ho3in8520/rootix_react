/** @type {import('next').NextConfig} */
const withTM = require('next-transpile-modules')(["@material-tailwind/react"]); 

module.exports =  withTM({
  reactStrictMode: true,
  images: {
    domains: ['blog.rootix.io', 'dl.rootix.io', 's2.coinmarketcap.com', 'rootix.io'],
  }
})
