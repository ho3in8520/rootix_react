const withMT = require("@material-tailwind/react/utils/withMT");


module.exports = withMT({
  mode: 'jit',
 
  content: [
    "./pages/**/*.{js,ts,jsx,tsx}",
    "./components/**/*.{js,ts,jsx,tsx}",

  ],
  darkMode: "class", 
  theme: {
    extend: {
       animation: {
          fadeIn: 'fadeIn 0.5s ease-in-out forward',
          wiggle: 'wiggle 2s ease-in-out infinite ',
        },
      keyframes: {
       fadeIn: {
        '0%': {opacity: '0'},
        '100%': {opacity: '1'}
       },
        wiggle: {
          '0%, 100%': { transform: 'translateX(10px)' },
          '50%': { transform: 'rotate(10deg)' },
        }
      },
      boxShadow: {
        '3xl': 'rgba(14, 30, 37, 0.12) 0px 2px 4px 0px, rgba(14, 30, 37, 0.32) 0px 2px 16px 0px',
       
      }
    },
    colors: {
      'blue': '#3734A9',
      'blue-dark': '#131066',
      'navi-blue': '#040D3E',
      'navi-blue-light' : '#111C44',
      'black': '#000000',
      'white': '#ffffff',
      'orange': '#F2994A',
      'orange-dark': '#ee7811',
      'purple': '#8476AA',
      'green' : '#2eb82e',
      'red' : '#e60000',
      'grey-dark': '#273444',
      'grey': '#E2E2FB',
      'grey-light': '#F8F8FC',
      'gold': '#F1A126',
    },
  },
  plugins: [

  ],

})
