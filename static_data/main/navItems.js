import { BriefcaseIcon, CurrencyDollarIcon, HomeIcon, InformationCircleIcon, NewspaperIcon, PhoneIcon, QuestionMarkCircleIcon } from "@heroicons/react/solid";

export default  [
    {label: "Rootix", href: "/", icon: <HomeIcon width={30} height={30}/>},
    {label: "Prices", href: "/prices", icon: <CurrencyDollarIcon width={30} height={30}/>},
    {label: "About Rootix", href: "/about-us", icon: <InformationCircleIcon width={30} height={30}/>},
    {label: "Contact Rootix", href: "/contact", icon: <PhoneIcon width={30} height={30}/>},
    // {label: "Services", href: "/services", icon: <BriefcaseIcon width={30} height={30}/>},
    {label: "FAQ", href: "#", icon: <QuestionMarkCircleIcon width={30} height={30}/>},
    {label: "Rootix Blog", href: "/blog", icon: <NewspaperIcon width={30} height={30}/>},
]