import _ from "lodash";

export const priceFormatter = (num) => {
    const integer = _.floor(num);
    const numWithTwoDecimal = _.floor(num, 2);
    const numWithFourDecimal = _.floor(num, 4);
    const numWithSevenDecimal = _.floor(num, 7);
    const numberOfDigits = integer.toString().length;

    if (!num) {
      return "-";
    } else if (num > 0 && num < 0.0009) {
      return numWithSevenDecimal + " $";
    } else if (num >= 0.0009 && num < 1) {
      return numWithFourDecimal + " $";
    } else if (numberOfDigits <= 3) {
      return numWithTwoDecimal.toLocaleString() + " $";
    } else if (numberOfDigits > 3 && numberOfDigits <= 6) {
      return integer.toLocaleString() + " $";
    } else if (numberOfDigits >= 7 && numberOfDigits < 10) {
      const millionFormat = _.floor(integer / 1000000, 2);
      return millionFormat.toLocaleString() + " M";
    } else if (numberOfDigits >= 10) {
      const billionFormat = _.floor(integer / 1000000000, 2);
      return billionFormat.toLocaleString() + " B";
    }
  };


