import _ from "lodash";

export const amountFormatter = (num) => {
    const integer = _.floor(num);
    const numWithTwoDecimal = _.floor(num, 2);
    const numWithFourDecimal = _.floor(num, 4);
    const numWithSevenDecimal = _.floor(num, 7);
    const numberOfDigits = integer.toString().length;

    if (!num) {
      return 0;
    } else if (num > 0 && num < 0.0009) {
      return numWithSevenDecimal;
    } else if (num >= 0.0009 && num < 1) {
      return numWithFourDecimal;
    } else if (num >= 1) {
      return numWithTwoDecimal
    }
  };

