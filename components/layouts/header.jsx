import { useState, useEffect, useRef } from "react";
import Link from "next/link";
import Image from "next/image";
import logoSm from "../../public/assets/images/logo-small.png";
import NavItem from "./header-components/navItem";
import navItems from "../../static_data/main/navItems";
import ThemeChanger from "../common/themeChanger";
import { MenuIcon } from "@heroicons/react/solid";


const Header = ({setIsOpen, page}) => {
  const [bgChange, setbgchange] = useState(false);
 

  const changeNavbarBg = () => {
    if (window.scrollY >= 80) {
      setbgchange(true);
    } else {
      setbgchange(false);
    }
  };

  useEffect(() => {
    window.addEventListener("scroll", changeNavbarBg);
  });

  return (
    
      <header
        className={`fixed top-0 w-full z-50  transition-colors duration-500 
        ${ page ==="main" && ( bgChange ? "bg-white dark:bg-navi-blue-light h-[80px] shadow-2xl" : "h-[110px]") } 
        ${ page !=="main" && ( bgChange ? "bg-white dark:bg-navi-blue-light h-[80px] shadow-2xl" : "h-[110px] bg-navi-blue pb-[40px]") } 
        `}
      >
        <nav
          className={`container h-full mx-auto flex justify-between ${
            bgChange ? "pt-0" : "pt-[40px]"
          }`}
        >
          <span className="cursor-pointer flex xl:hidden items-center">
            <MenuIcon
              width={50}
              height={50}
              color={bgChange ? "navi-blue" : "white"}
              onClick={() => setIsOpen(true)}
            />
          </span>
          
          <div className="h-full w-[80px] flex items-center justify-center absolute xl:static top-1/2 
          left-1/2 transform -translate-x-1/2 -translate-y-1/2 xl:translate-y-0 xl:translate-x-0 xl:transform-none"
          >
            <Link href="/">
              <a className="flex justify-center items-center">
                <Image src={logoSm} alt="" />
              </a>
            </Link>
          </div>

          <div className="h-full flex-auto px-6 items-center hidden xl:flex">
            <ul className="flex text-black">
              {navItems?.map((item) => (
                <NavItem
                  key={item.label}
                  label={item.label}
                  href={item.href}
                  color={bgChange ? "black" : "white"}
                  exact={item.href === "/" ? true : false}
                />
              ))}
            </ul>
          </div>

          <div className="h-full flex-none w-[250px] flex items-center justify-end">
            <ThemeChanger />
            {/* <LangSelector defaultLang="EN" color={bgChange ? "black" : "white"} /> */}
            <Link href="/login">
              <a
                className={`btn px-4 py-2 text-white bg-orange border-none rounded min-w-[150px] text-center transition duration-300 hover:bg-orange-dark hidden md:block`}
              >
                login/register
              </a>
            </Link>
          </div>

        </nav>
      </header>
   
  );
};

export default Header;
