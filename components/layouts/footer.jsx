import Image from "next/image";
import Link from "next/link";
import logo from "../../public/assets/images/logo-small.png";
import linkdinIcn from "../../public/assets/icons/linkedin.png";
import youtubeIcn from "../../public/assets/icons/youtube.png";
import instaIcn from "../../public/assets/icons/insta.png";
import twitterIcn from "../../public/assets/icons/twitter.png";
import { useForm } from "react-hook-form";
import Swal from "sweetalert2";
import axios from "axios";

import instagramLogo from "../../public/assets/icons/colorfull-instagram.png";
import twiiterLogo from "../../public/assets/icons/colorfull-twitter.png";
import facebookLogo from "../../public/assets/icons/colorfull-facebook.png";

const Footer = () => {


  return (
    <footer className="bg-navi-blue min-h-[500px] mt-[50px] py-[70px] dark:mt-0 dark:pt-[120px]">
      <div className="container mx-auto">
        <div className="flex flex-col md:flex-row w-full border-b-2 border-white pb-[30px] justify-between items-center md:items-between">
          <div className="w-[250px] flex flex-col">
            <div className="flex flex-col md:flex-row justify-between items-center">
              <Image src={logo} alt="rootix" width={71} height={71}></Image>
              <h2 className="text-white text-4xl mt-[10px] md:mt-0">Rootix</h2>
            </div>
            <p className="mt-[10px] md:mt-[30px] text-white text-base text-center md:text-left">
              Finance helps companies manage payments easily.
            </p>
            <div className="p-4 gap-x-4 mt-[50px] hidden md:flex">
              <Link href="#">
                <a className="mb-2">
                  <Image src={linkdinIcn} alt="" width={25} height={25} />
                </a>
              </Link>
              <Link href="#">
                <a className="mb-2">
                  <Image src={instaIcn} alt="" width={25} height={25} />
                </a>
              </Link>
              <Link href="#">
                <a className="mb-2">
                  <Image src={twitterIcn} alt="" width={25} height={25} />
                </a>
              </Link>
              <Link href="#">
                <a>
                  <Image src={youtubeIcn} alt="" width={25} height={25} />
                </a>
              </Link>
            </div>
          </div>
          <div className="w-[350px] flex md:hidden xl:flex justify-between mt-[30px] xl:mt-0">
            <div className="w-fit xl:basis-1/2 text-white">
              <h3 className="text-lg font-semibold">Rootix</h3>
              <ul className="text-sm flex flex-col mt-[40px] gap-y-7">
                <li>
                  <Link href="#">
                    <a className="hover:text-gold">About Rootix</a>
                  </Link>
                </li>
                <li>
                  <Link href="#">
                    <a className="hover:text-gold">Rootix Team</a>
                  </Link>
                </li>{" "}
                <li>
                  <Link href="#">
                    <a className="hover:text-gold">About Rootix</a>
                  </Link>
                </li>{" "}
                <li>
                  <Link href="#">
                    <a className="hover:text-gold">Prices</a>
                  </Link>
                </li>
              </ul>
            </div>

            <div className="w-fit xl:basis-1/2 text-white">
              <h3 className="text-lg font-semibold">Rootix</h3>
              <ul className="text-sm flex flex-col mt-[40px] gap-y-7">
                <li>
                  <Link href="#">
                    <a className="hover:text-gold">About Rootix</a>
                  </Link>
                </li>
                <li>
                  <Link href="#">
                    <a className="hover:text-gold">Rootix Team</a>
                  </Link>
                </li>{" "}
                <li>
                  <Link href="#">
                    <a className="hover:text-gold">About Rootix</a>
                  </Link>
                </li>{" "}
                <li>
                  <Link href="#">
                    <a className="hover:text-gold">Prices</a>
                  </Link>
                </li>
              </ul>
            </div>
          </div>
          <div className="w-[400px] flex flex-col">
            <h4 className="text-white text-2xl text-center md:text-left mt-[50px] md:mt-0">
              Subscribe to our newsletter!
            </h4>

            <Newsletter />

            <p className="text-purple hidden md:flex">
              The first part of the tutorial: Lorem ipsum dolor sit amet,
              consectetur adipiscing elit
            </p>
          </div>
        </div>

        <div className="flex flex-col w-full pt-[40px]">
          <div className="md:flex gap-x-[30px] mx-auto hidden">
            <Link href="#">
              <a className="border-b border-purple text-purple hover:text-gold hover:border-gold transition duration-500">
                Twitter
              </a>
            </Link>
            <Link href="#">
              <a className="border-b border-purple text-purple hover:text-gold hover:border-gold transition duration-500">
                Dribble
              </a>
            </Link>
            <Link href="#">
              <a className="border-b border-purple text-purple hover:text-gold hover:border-gold transition duration-500">
                Instagram
              </a>
            </Link>
            <Link href="#">
              <a className="border-b border-purple text-purple hover:text-gold hover:border-gold transition duration-500">
                YouTube
              </a>
            </Link>
            <Link href="#">
              <a className="border-b border-purple text-purple hover:text-gold hover:border-gold transition duration-500">
                Slack
              </a>
            </Link>
          </div>

          <div className="flex gap-x-[30px] mx-auto md:hidden">
            <Link href="#">
              <a>               
                <Image
                  src={instagramLogo}
                  width={30}
                  height={30}
                  alt="Rootix instagram"
                />
              </a>
            </Link>

            <Link href="#">
              <a>
                <Image
                  src={twiiterLogo}
                  width={30}
                  height={30}
                  alt="Rootix instagram"
                />
              </a>
            </Link>

            <Link href="#">
              <a>
                {" "}
                <Image
                  src={facebookLogo}
                  width={30}
                  height={30}
                  alt="Rootix instagram"
                />
              </a>
            </Link>
          </div>

          <div className="flex flex-col-reverse md:flex-row mx-auto mt-[40px] items-center gap-y-[15px]">
            <p className="md:border-r border-purple md:pr-[50px] text-purple">
              2022 All Rights Reserved
            </p>
            <Link href="mailto:info@rootix.io">
              <a className="md:border-r border-purple md:px-[50px] text-purple hover:text-gold hover:border-gold transition duration-500">
                info@rootix.io
              </a>
            </Link>
            <Link href="tel:+923008489895">
              <a className="md:pl-[50px] text-purple hover:text-gold hover:border-gold transition duration-500">
                +92 300 848 9895
              </a>
            </Link>
          </div>
        </div>
      </div>
    </footer>
  );
};

export default Footer;

const Newsletter = () => {
  const {
    register,
    handleSubmit,
    reset,
    formState: { errors },
  } = useForm();

  const submitEmail = async (data) =>{

    const apiUrl = process.env.NEXT_PUBLIC_API_URL;
    try{
       const response = await axios({
      method: "post",
      url: `${apiUrl}/newsletter`,
      data,
    });
if (response.status === 200 && response.data.message === "successfully done.") {
      Swal.fire({
        icon: "success",
        title: "Done",
        text: "Your Email successfully has submitted.",
        confirmButtonText: "Ok",
        timer: 3000,
        width: "350",
      });
  
      reset({
        email: "",
      });
    }
   
    }catch(e){
      console.log(e);
      Swal.fire({
              icon: "error",
              title: "Ooops..",
              text: e.response.data.message || "Something went wrong...",
              confirmButtonText: "Ok",
              timer: 3000,
              width: "350",
            });
            reset({
              email: "",
            });
    }
  }
  
  return (
    <>
      <form
        onSubmit={handleSubmit(submitEmail)}
        className="my-[20px] md:my-[40px]"
      >
        <div className="flex jsutify-center items-center shadow-3xl bg-white">
          <input
            className="w-full border-none h-full rounded focus:outline-0 text-sm text-xl px-5 bg-white p-3 dark:text-grey-dark text-grey-dark"
            type="email"
            id="email"
            name="email"
            {...register("email", {
              required: true,
              pattern: /^\w.+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$/,
            })}
          />
          <button
            className="text-center px-3 bg-white h-full flex items-center font-medium text-xl bg-[#00E1F0]
           duration-500 hover:bg-[#00a7b3] transition text-white py-3 px-[45px] cursor-pointer hover:"
            type="submit"
          >
            send
          </button>
        </div>
        {errors.email?.type === "required" && (
          <p className="text-red text-sm italic mt-3 font-medium">
            The Email field can not be empty.
          </p>
        )}
        {errors.email?.type === "pattern" && (
          <p className="text-red text-sm italic mt-3 font-medium">
            Please enter a valid Email address.
          </p>
        )}
      </form>
    </>
  );
};
