import Link from "next/link";
import { useRouter } from 'next/router'

const NavItem = ({label, href, exact, color, className, navStyle, icon}) => {
    const { pathname } = useRouter();
    const navColor = color;
    const isActive = exact ? pathname === href : pathname.startsWith(href);
  return (
    <li className={`
    ${isActive ? `text-orange font-semibold ${className}` : `text-${navColor} ${className} dark:text-white` }
    px-3 hover:text-gold transition list-none`}
    style={navStyle}
    >
      <Link href={href}>
        <a className="flex gap-x-5 justify-center items-center hover:text-gold">{icon}{label}</a>
      </Link>
    </li>
  );
};

export default NavItem;
