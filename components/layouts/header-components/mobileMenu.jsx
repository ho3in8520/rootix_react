import { useRef, useEffect, useState } from 'react';
import style from '../layout.module.css';
import navItems from '../../../static_data/main/navItems';
import NavItem from './navItem';
import Link from 'next/link';
import {XCircleIcon} from "@heroicons/react/solid";

function MobileMenu({isOpen,setIsOpen }) {

    return (
        <div className={`fixed h-screen z-[60] w-full bg-white ${isOpen ? "left-0" : "left-[-100%]"} top-0 bottom-0 shadow-3xl xl:hidden ${style.mobileMenuBg} ${style.mobileMenuTransition}`}>
            <span className='fixed right-[40px] top-[40px] cursor-pointer z-[500]' onClick={()=> setIsOpen(false)}> <XCircleIcon width={50} height={50} color="gold"/></span>
           
            <ul className="flex flex-col justify-around items-center w-[200px] py-[50px] ease-linear absolute h-screen top-1/2 left-1/2 transform -translate-x-1/2 -translate-y-1/2 divide-y">
            {isOpen && navItems?.map((item, index )=> (
               <NavItem
               key = {index}
              label={item.label}
              href= {item.href}
              icon={item.icon}
              className=  {`animate-ping hover:text-gold py-5 w-[200px] flex`}
              color="white"
              navStyle={{animationDelay: 500 + (index * 100) + "ms", animationIterationCount: "1"}}
              exact = {item.href === "/" ? true : false}
            />)
           )}
          {isOpen && (
            <li className='border-none'>
             <Link href="/login">
             <a
               className={`px-4 py-2 text-white bg-orange border-none rounded min-w-[150px] text-center transition duration-300 hover:bg-orange-dark block md:hidden`}
             >
               login/register
             </a>
           </Link>
           </li>
           )}
             </ul>
        </div>
    );
}

export default MobileMenu;