import { useEffect, useState } from 'react';
import { ParallaxProvider } from 'react-scroll-parallax'; 
import style from "./layout.module.css";
import Meta from './Meta';
import Header from "./header";
import MobileMenu from "./header-components/mobileMenu";
import Footer from "./footer";


const Layout = ({ children, page }) => {

  const [mounted,  setMounted] = useState(false);
  const [isOpen, setIsOpen] = useState(false);
  useEffect(() =>{
    setMounted(true)
  }, [])
  
  let theme = mounted ? localStorage.getItem(theme) : null;

  return (
    <ParallaxProvider>
       <Meta />
      <div className={`${theme} relative`}>
        <Header style={style} setIsOpen={setIsOpen} page={page}/>
        <MobileMenu isOpen={isOpen} setIsOpen={setIsOpen} />
        <main className="bg-white dark:bg-navi-blue">{children}</main>
        <Footer />
      </div>
    </ParallaxProvider>
  );
};

export default Layout;
