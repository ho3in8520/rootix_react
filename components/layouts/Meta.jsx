import React from "react";
import Head from "next/head";

function Meta({
  title = " Rootix-online crypto exchange",
  description = `Routix, a safe market for buying and selling Bitcoin, Ethereum and digital currencies, is proud to serve you dear ones by facilitating trading and having the standard features of the world's top digital currency exchanges in a professional environment.`
}) {
  return (
    <Head>
      <meta charSet="UTF-8" />
      <meta name="viewport" content="width=device-width, initial-scale=1.0" />
      <meta name="author" content="rootix-team" />
      <meta name="robots" content="index/follow" />
      <meta name="description" content={description} />
      <title>{title}</title>
    </Head>
  );
}

export default Meta;
