import React from "react";
import LineChart from "../../components/common/lineChart";
import _ from "lodash";
import Link from "next/link";
import Image from "next/image";
import LineSkeleton from "./lineSkeleton";
import {priceFormatter} from "../../utils/priceFormatter";

function Table({ data, timeFrame }) {
  const generateRandomData = () => {
    const chartData = Array.from({ length: 10 }, () =>
      Math.floor(Math.random() * 50)
    );
    return chartData;
  };

  const loadingArr = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
  return (
    <div className=" border border-grey dark:border-[#ffffff33] rounded-xl mt-[50px] ">
      <table className=" border-collapse w-full rounded-full">
        <thead className="h-[100px]">
          <tr className="py-[20px] border-b border-grey dark:border-[#ffffff33] items-center text-[#666666]">
            <th className="text-[18px] font-semibold text-left pl-[20px] md:pl-[50px] w-[140px] md:w-[220px] ">
              Coins
            </th>
            <th className="text-[18px] font-semibold text-center">Price</th>
            <th className="text-[18px] font-semibold text-center">Change</th>
            <th className="text-[18px] font-semibold text-center w-[150px] hidden lg:table-cell">
              Chart
            </th>
            <th className="text-[18px] font-semibold text-center hidden lg:table-cell">
              Volume
            </th>
            <th className="text-[18px] font-semibold text-center hidden lg:table-cell">
              Marketcap
            </th>
            <th className="text-[18px] font-semibold text-center hidden lg:table-cell"></th>
          </tr>
        </thead>

        <tbody>
          {data.length
            ? data?.map((row) => {
                const period =
                  timeFrame === "_1h"
                    ? row?._1h
                    : timeFrame === "_1d"
                    ? row?._1d
                    : timeFrame === "_7d"
                    ? row?._7d
                    : timeFrame === "_30d"
                    ? row?._30d
                    : row?._90d;
                priceFormatter(row.market_cap);
                return (
                  <tr
                    key={row?.price}
                    className="w-full border-b border-grey dark:border-[#ffffff33] items-start h-[100px]"
                  >
                    <td className="text-left pl-[20px] md:pl-[30px]">
                      <div className="flex gap-x-[10px] items-center justify-start mx-auto">
                        {row?.logo_url && (
                          <Image
                            src={row?.logo_url}
                            alt=""
                            width={35}
                            height={35}
                            className="rounded-full"
                          />
                        )}
                        <div className="flex md:flex-col flex-col-reverse">
                          <p className="md:text-[17px] text-[10px]">
                            {row?.name}
                          </p>
                          <p className="md:text-[12px] md:text-[#666666]">
                            {row?.currency}
                          </p>
                        </div>
                      </div>
                    </td>

                    <td className="text-center">{priceFormatter(row?.price)}</td>
                    <td className="text-center">
                      <p
                        className={
                          period > 0
                            ? "text-green"
                            : period < 0
                            ? "text-red"
                            : "text-grey"
                        }
                      >
                        {_.floor(period, 2)} %
                      </p>
                    </td>
                    <td className="text-center w-[150px] h-[100px] hidden lg:table-cell">
                      <div className="h-[50px]">
                        <LineChart
                          borderWidth={2}
                          data={generateRandomData()}
                          trend={period > 0 ? "bullish" : "bearish"}
                        />
                      </div>
                    </td>
                    <td className="text-center hidden lg:table-cell">
                      {priceFormatter(row?.volume)}
                    </td>
                    <td className="text-center hidden lg:table-cell">
                      {priceFormatter(row?.market_cap)}
                    </td>
                    <td className="text-center hidden lg:table-cell">
                      <Link href={"#"}>
                        <a
                          className="cursor-pointer mx-auto px-3 py-3 rounded-md
                           text-white bg-blue border-none rounded w-full text-center transition
                            duration-300 hover:bg-blue-dark w-[100px] flex justify-center"
                        >
                          Trade
                        </a>
                      </Link>
                    </td>
                  </tr>
                );
              })
            : loadingArr.map((item) => (
                <tr
                  key={item}
                  className="w-full border-b border-grey items-center h-[100px]"
                >
                  <td className="text-center">
                    <LineSkeleton width={150} />
                  </td>
                  <td className="text-center">
                    <LineSkeleton width={150} />
                  </td>
                  <td className="text-center">
                    <LineSkeleton width={150} />
                  </td>
                  <td className="text-center">
                    <LineSkeleton width={150} />
                  </td>
                  <td className="text-center">
                    <LineSkeleton width={150} />
                  </td>
                  <td className="text-center">
                    <LineSkeleton width={150} />
                  </td>
                  <td className="text-center">
                    <LineSkeleton width={150} />
                  </td>
                </tr>
              ))}
        </tbody>
      </table>
    </div>
  );
}

export default Table;
