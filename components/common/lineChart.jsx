import React, { useState, useEffect } from "react";
import gradient from "chartjs-plugin-gradient";

import {
  Chart as ChartJS,
  LineElement,
  PointElement,
  LineController,
  CategoryScale,
  LinearScale,
  Filler,
  Legend,
} from "chart.js";
import { Line } from "react-chartjs-2";

ChartJS.register(
  gradient,
  LineElement,
  PointElement,
  LineController,
  CategoryScale,
  LinearScale,
  Filler,
  Legend
);

function LineChart({data, trend, borderWidth= 0}) {

  const chartOptions = {
    maintainAspectRatio: false,
    responsive: true,
    plugins: {
      legend: {
        display: false,
      },
      title: {
        display: false,
      },

      point: {
        display: false,
      },
    },
    scales: {
      xAxis: {
        display: false,
      },
      yAxis: {
        display: false,
      },
    },
  };

  const dividedData = data && data[0] > 100 ? data.map(n => (n/1000)) : data;

  const chartData = {
    labels:dividedData,
    datasets: [
      {
        lineTension: 0.4,
        gradient: {
          backgroundColor: {
            axis: "y",
            colors: trend ==="bullish" ? {
              0: "rgba(0,230,0,0)",
              50: "rgba(0,230,0,0.8)",
              100: "rgba(0,230,0,1)",
            } : {
              0: "rgba(255,0,0,0)",
              50: "rgb(255, 0, 0, 0.8)",
              100: "rgb(255, 0, 0 , 1)",
            },
          },
        },
        id: 1,
        label: "",
        data: dividedData,
        borderColor : trend === "bullish" ? "#009926" : "#e60000",
        fill: "start",
        
        radius: 0,
        borderWidth,
        hitRadius: 5,
      },
    ],
  };

  
  return (
    <Line
      type="line"
      data={chartData}
      options={chartOptions}
      width={50}
      height={60}
    />
  );
}

export default LineChart;
