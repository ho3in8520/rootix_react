import { ChevronDownIcon } from "@heroicons/react/solid";
import enFlag from "../../public/assets/images/flag-en.svg";
import faFlag from "../../public/assets/images/flag-fa.svg";
import Image from "next/image";
import { useState } from "react";
import Link from "next/link";

const LangSelector = ({ defaultLang, color }) => {
  const langs = [
    { label: "EN", flag: enFlag,
     href: "/" },
    { label: "FA", flag: faFlag, href: "/" },
  ];




  const [open, setOpen] = useState(false);
  const [lang, setLang] = useState({
    label: langs.filter((lang) => lang.label === defaultLang)[0].label,
    flag: langs.filter((lang) => lang.label === defaultLang)[0].flag,
  });

const langSelector = (item) =>{
  setOpen(false);
  setLang({label: item.label, flag: item.flag})
}
  const langColor = color;
  const visibility = open ? "flex opacity-1" : "hidden opacity-0";
  return (
    <div className="relative h-full mx-3">
      <div
        className={`flex w-[70px] h-full cursor-pointer text-${color} justify-between items-center`}
        onClick={() => setOpen(!open)}
      >
        <Image src={lang.flag} alt="" height="20" width="20"></Image>
        <span>{lang.label}</span>
        <ChevronDownIcon className="w-[20px]" />
      </div>

      <div
        className={`
        bg-clip-padding backdrop-filter backdrop-blur-xl bg-opacity-60
        absolute -bottom-[80px] right-[50px] bg-white rounded transition ease-linear duration-500 transition-opacity ${visibility}`}
      >
        <ul className="text-black divide-y divide-grey-light">
          {langs.map((item) => (
            <li onClick={() => langSelector(item)} key={item.label}>
              <Link href={item.href}>
                <a className="flex rounded justify-between items-center w-[180px] py-3 px-5 hover:bg-grey transition-colors duration-300	">
                  <span>{item.label}</span>
                  <Image src={item.flag} alt="" height="25" width="25"></Image>
                </a>
              </Link>
            </li>
          ))}
        </ul>
      </div>
    </div>
  );
};

export default LangSelector;
