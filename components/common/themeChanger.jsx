import { useTheme } from "next-themes";
import { useState, useEffect } from "react";
import { MoonIcon, SunIcon, DesktopComputerIcon } from "@heroicons/react/solid";

const ThemeChanger = () => {
  const { theme, setTheme } = useTheme();
  const [mounted, setMounted] = useState(false);

  const [tooltip, setTooltip] = useState(false);
  useEffect(() => {
    setMounted(true);
  }, []);
  if (!mounted) return null;


  return (
    <div>
      <div
        className="flex relative mx-5"
        onMouseOver={() => setTooltip(true)}
        onMouseLeave={() => setTooltip(false)}
        onClick={() => setTooltip(false)}
      >
        {theme === "dark" ? (
          <span
            className="cursor-pointer"
            onClick={() => setTheme("light")}
          >
            <MoonIcon width={25} height={25} color={"#ffff80"} />
          </span>
        ) : theme === "light" ? (
          <span
            className="cursor-pointer"
            onClick={() => setTheme("system")}
          >
            <SunIcon width={25} height={25} color={"gold"} />
          </span>
        ) : (
          <span
            className="cursor-pointer"
            onClick={() => setTheme("dark")}
          >
            <DesktopComputerIcon width={25} height={25} color={"grey"} />
          </span>
        )}
        {
          <div
            className={`${
              tooltip ? "opacity-1" : "opacity-0"
            } absolute px-5 py-2 top-[40px] w-fit bg-white rounded shadow-3xl text-center text-xs transition-all ease-linear`}
          >
            <p className="w-[80px] dark:text-grey-dark">theme: {theme}</p>
          </div>
        }
      </div>
    </div>
  );
};

export default ThemeChanger;
