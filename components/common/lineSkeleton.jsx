import React from "react";
import {useTheme} from "next-themes";
import dynamic from 'next/dynamic'
const ContentLoader = dynamic(() => import('react-content-loader'), {
  ssr:false
})

function LineSkeleton({ width }) {

  const {resolvedTheme} = useTheme();

  return (
    <>
      <ContentLoader
        speed={2}
        width={width}
        height={100}
        viewBox="0 0 320 310"
        backgroundColor={resolvedTheme === "light"  ? "#f3f3f3" : "#2b3053"}
        foregroundColor={resolvedTheme === "light" ? "#d9d9d9" : "#111C44"}
      >
        <rect x="0" y="130" rx="10" ry="10" width={width} height="30" />
      </ContentLoader>
    </>
  );
}

export default LineSkeleton;
