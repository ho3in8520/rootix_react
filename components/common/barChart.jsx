import React, { Component } from "react";
import gradient from "chartjs-plugin-gradient";
import { Bar } from "react-chartjs-2";

import {
    Chart,
    ArcElement,
    LineElement,
    BarElement,
    PointElement,
    BarController,
    BubbleController,
    DoughnutController,
    LineController,
    PieController,
    PolarAreaController,
    RadarController,
    ScatterController,
    CategoryScale,
    LinearScale,
    LogarithmicScale,
    RadialLinearScale,
    TimeScale,
    TimeSeriesScale,
    Decimation,
    Filler,
    Legend,
    Title,
    Tooltip,
    SubTitle
  } from 'chart.js';
  
  Chart.register(
    ArcElement,
    LineElement,
    BarElement,
    PointElement,
    BarController,
    BubbleController,
    DoughnutController,
    LineController,
    PieController,
    PolarAreaController,
    RadarController,
    ScatterController,
    CategoryScale,
    LinearScale,
    LogarithmicScale,
    RadialLinearScale,
    TimeScale,
    TimeSeriesScale,
    Decimation,
    Filler,
    Legend,
    Title,
    Tooltip,
    SubTitle
  );

class BarChart extends Component {
  state = {

    /* chart options */
    chartOpts: {
      maintainAspectRatio: false,
      responsive: true,
      plugins: {
        legend: {
          display: false,
        },
        title: {
          display: false,
        },

        point: {
          display: false,
        },
      },
      scales: {
        xAxis: {
          display: false,
        },
        yAxis: {
          display: false,
        },
      },
    },


    /* chart data */
    chartData : {
      labels: ["x", "y", "z", "x", "y", "z"],
      datasets: [
        {
          gradient: {
            backgroundColor: {
              axis: "y",
              colors: this.props.bg === "green" ? {
                0: "rgba(255,255,255,0.6)",
                50: "#68E0C0",
                100: "#00b300",
              } : {
                0: "rgba(231,0,0,0.1)",
                50: "#ff9999",
                100: "#ffe6e6",
              },
            },
          },
          id: 1,
          label: "",
          data: this.props.data,
          borderColor: "#009926",
          fill: "start",
          backgroundColor: "rgba(97, 222, 189, 0.7)",
          radius: 0,
          borderWidth: 0,
          hitRadius: 0,
        },
      ],
    }
  };

  render() {
   

    const { chartData, chartOpts } = this.state;
    return (
      <Bar
        type="bar"
        data={chartData}
        options={chartOpts}
        width={60}
        height={60}
      />
    );
  }
}

export default BarChart;
