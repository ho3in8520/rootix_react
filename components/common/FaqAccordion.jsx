import React, { useState } from "react";
import {
  Accordion,
  AccordionHeader,
  AccordionBody,
} from "@material-tailwind/react";
import { useEffect } from "react";

function FaqAccordion({question, children, num }) {
  const [open, setOpen] = useState(0);

  const handleOpen = (value) => {
    setOpen(open === value ? 0 : value);
  };
    useEffect(() =>{
    setOpen(1)
    }, [])
  return (
    <>
      <Accordion open={open === num} onClick={() => handleOpen(num)} className="" >
        <AccordionHeader className="border-b border-grey pb-[30px] dark:!text-white">{question}</AccordionHeader>
        <AccordionBody className="dark:text-purple">
         {children}
        </AccordionBody>
      </Accordion>
    </>
  );
}

export default FaqAccordion;
