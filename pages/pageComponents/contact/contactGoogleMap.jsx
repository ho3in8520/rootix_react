import React, { useState, useEffect } from "react";
import Map from 'react-map-gl';
import 'mapbox-gl/dist/mapbox-gl.css';
import {useWindowSize} from "../../../utils/windowSize";

function ContactGoogleMap(props) {
  const size = useWindowSize();

  return (
    <Map
    mapboxAccessToken={process.env.NEXT_PUBLIC_MAP_API_KEY}
    initialViewState={{
      longitude: -122.4,
      latitude: 37.8,
      zoom: 14
    }}
    style={{width: "95%", height: size.width >= 960 ? "100%" : "500px", marginRight: "auto", marginLeft: "auto", borderRadius: "5px"}}
    mapStyle="mapbox://styles/mapbox/streets-v9"
  />
 
) 
}

export default ContactGoogleMap;
