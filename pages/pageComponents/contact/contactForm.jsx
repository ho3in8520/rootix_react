import React, { useState } from "react";
import { MailIcon } from "@heroicons/react/solid";
import { useForm } from "react-hook-form";

function ContactForm(props) {
  const {
    register,
    handleSubmit,
    reset,
    formState: { errors },
  } = useForm();

  const onSubmit =  (data) => {
    console.log(data);
    // const response = await axios({
    //   method: "post",
    //   url: "https://www.api.payascript.ir/wp-json/contact-form-7/v1/contact-forms/54/feedback",
    //   data: formedData,
    // });

    // if (response.status === 200 && response.data.status === "mail_sent") {
    //   Swal.fire({
    //     icon: "success",
    //     title: `${data.fullName} عزیز پیام شما با موفقیت ارسال شد.`,
    //     confirmButtonText: "حله ...",
    //     timer: 3000,
    //     width: "350",
    //   });

    //   reset({
    //     fullName: "",
    //     email: "",
    //     phoneNumber: "",
    //     subject: "",
    //     description: "",
    //   });
    // }else if(response.status === 200 && response.data.status === "validation_failed"){
    //   {
    //     Swal.fire({
    //       icon: "error",
    //       title: response.data.message,
    //       confirmButtonText: "باشه ...",
    //       timer: 3000,
    //       width: "350",
    //     });
    //   }
    // }else if(response.status !== 200){
    //   {
    //     Swal.fire({
    //       icon: "error",
    //       title: "متاسفانه مشکلی در ارسال پیام وجود دارد لطفا مجددا تلاش کنید",
    //       confirmButtonText: "باشه ...",
    //       timer: 3000,
    //       width: "350",
    //     });
    //   }
    // }

 
  };
  return (
    <>
      <div className="flex gap-x-4 items-center">
        <h3 className="font-bold text-center text-2xl sm:text-3xl lg:text-4xl leading-normal lg:leading-relaxed text-grey-dark dark:text-white">
          Contact Us
        </h3>
        <MailIcon width={30} />
      </div>
      <p className="text-purple my-[40px]">
        The first part of the tutorial: Lorem ipsum dolor sit amet, consectetur
        adipiscing elit, Lorem ipsum dolor sit amet
      </p>

      <form className="px-[30px]"
       id="contact-form"
       onSubmit={handleSubmit(onSubmit)}
      >
        <div className="flex gap-x-[20px] flex-col sm:flex-row">
          <div className="basis-1/2 flex flex-col">
            <label className="text-grey-dark dark:text-white" htmlFor="name">
              Name *
            </label>
            <input
              className="md:border-b md:border-[#cccccc] md:shadow-none border-0 shadow-3xl rounded py-[10px] md:py-0 focus:outline-none focus:border-gold mt-4 dark:bg-navi-blue-light dark:md:bg-transparent md:bg-transparent"
              name="name"
              id="name"
              {...register("name", { required: false, maxLength: 20 })}
            />
            {errors.name?.type === "required" && (
              <p className="text-red text-sm italic mt-3 font-medium">Please enter your name</p>
            )}
            {errors.name?.type === "maxLength" && (
              <p className="text-red text-sm italic mt-3 font-medium">Please enter your name correctly</p>
            )}
          </div>
          <div className="basis-1/2 flex flex-col mt-[50px] sm:mt-0">
            <label className="text-grey-dark dark:text-white" htmlFor="fullName">
              Last Name *
            </label>
            <input
              className="md:border-b md:border-[#cccccc] md:shadow-none border-0 shadow-3xl rounded py-[10px] md:py-0 focus:outline-none focus:border-gold mt-4 dark:bg-navi-blue-light dark:md:bg-transparent md:bg-transparent "
              name="lastName"
              id="lastName"
              {...register("lastName", { required: false, maxLength: 20 })}
            />
            {errors.lastName?.type === "required" && (
              <p className="form-error">Please enter your Last Name </p>
            )}
            {errors.lastName?.type === "maxLength" && (
              <p className="form-error">
                Please enter your Last Name correctly
              </p>
            )}
          </div>
        </div>

        <div className="flex gap-x-[20px] mt-[50px] flex-col sm:flex-row">
          <div className="basis-1/2 flex flex-col">
            <label className="text-grey-dark dark:text-white " htmlFor="phoneNumber">
              Phone Number
            </label>
            <input
              className="md:border-b md:border-[#cccccc] md:shadow-none border-0 shadow-3xl rounded py-[10px] md:py-0 focus:outline-none focus:border-gold mt-4 dark:bg-navi-blue-light dark:md:bg-transparent md:bg-transparent "
              name="phoneNumber"
              id="phoneNumber"
              {...register("phoneNumber", { minLength: 14, maxLength: 14 })}
            />
            {errors.email?.type === "required" && (
              <p className="form-error">Please enter your Email address</p>
            )}
            {errors.email?.type === "pattern" && (
              <p className="form-error">
                Please enter your Email address correctly
              </p>
            )}
          </div>
          <div className="basis-1/2 flex flex-col mt-[50px] sm:mt-0">
            <label className="text-grey-dark dark:text-white" htmlFor="email">
              Email *
            </label>
            <input
              className="md:border-b md:border-[#cccccc] md:shadow-none border-0 shadow-3xl rounded py-[10px] md:py-0 focus:outline-none focus:border-gold mt-4 dark:bg-navi-blue-light dark:md:bg-transparent md:bg-transparent"
              id="email"
              name="email"
              {...register("email", {
                required: true,
                pattern: /^\w.+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$/,
              })}
            />
            {errors.fullName?.type === "required" && (
              <p className="form-error">Please enter your Last Name </p>
            )}
            {errors.fullName?.type === "maxLength" && (
              <p className="form-error">
                Please enter your Last Name correctly
              </p>
            )}
          </div>
        </div>

        <div className="w-full flex flex-col mt-[50px]">
          <label className="" htmlFor="description">
            Message *
          </label>
          <textarea
            className="md:border md:border-[#cccccc] md:shadow-none border-0 shadow-3xl mt-4 h-[200px] focus:outline-none focus:border-gold p-3 rounded dark:bg-navi-blue-light dark:md:bg-transparent md:bg-transparent"
            name="description"
            id="description"
            {...register("description")}
          ></textarea>
        </div>

        <div className="w-full flex justify-center">
          <button
            name="submit"
            type="submit"
            id="submit"
            className="
          mt-[50px] bg-gradient-to-r from-[#38B8F2] to-[#7F44F6] px-[50px] py-[13px] 
          text-white rounded transition-colors hover:from-[#7F44F6] hover:to-[#38B8F2] "
          >
            Send Message
          </button>
        </div>
      </form>
    </>
  );
}

export default ContactForm;
