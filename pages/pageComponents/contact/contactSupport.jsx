import React from "react";
import Image from "next/image";
import avatar01 from "../../../public/assets/images/avatar.png";
import avatar02 from "../../../public/assets/images/avatar02.png";
import avatars from "../../../public/assets/images/avatars.png";
import circles from "../../../public/assets/images/circles.png";


const ContactSupport = () => {
  return (
    <div className="w-full pt-[200px]">
      <div className="container mx-auto">
        <div className="flex relative flex-col-reverse lg:flex-row">
          <span className="absolute bottom-0 left-0 z-10">
            <Image src={circles} width={150} height={150} alt=""/>
          </span>
          <div className="basis-1/2">
            <div className="w-[90%] mx-auto bg-white dark:bg-navi-blue-light p-5 relative h-[500px] rounded-xl">
              <div className="flex gap-x-5 items-end absolute top-[30px] left-5 z-20 opacity-0 fadeInAnimation">
                <Image
                  className="rounded-full"
                  src={avatar02}
                  width={40}
                  height={40}
                  alt=""
                />
                <div className="flex flex-col gap-y-2">
                  <div className="rounded-r-xl rounded-tl-xl min-w-[200px] h-[60px] bg-white shadow-3xl flex justify-center items-center px-5 text-purple">
                    Hi John, How can I help you?
                  </div>
                  <span className="text-purple">12:35 AM</span>
                </div>
              </div>

              <div className="flex flex-row-reverse gap-x-5 items-end absolute top-[160px] right-5 z-20 opacity-0 fadeInAnimation" style={{animationDelay: "0.15s"}}>
                <Image
                  className="rounded-full"
                  src={avatar01}
                  width={40}
                  height={40}
                  alt=""
                />
                <div className="flex flex-col items-end gap-y-2">
                  <div className="rounded-l-xl rounded-tr-xl min-w-[200px] h-[60px] bg-white shadow-3xl flex justify-center items-center px-5 text-purple bg-navi-blue">
                    The assets didnt deposit to my wallet !!!
                  </div>
                  <span className="text-purple">12:37 AM</span>
                </div>
              </div>

              <div className="flex gap-x-5 items-end absolute top-[290px] left-5 z-20 opacity-0 fadeInAnimation" style={{animationDelay: "0.3s"}}>
                <Image
                  className="rounded-full"
                  src={avatar02}
                  width={40}
                  height={40}
                  alt=""
                />
                <div className="flex flex-col gap-y-2">
                  <div className="rounded-r-xl rounded-tl-xl min-w-[200px] h-[60px] bg-white shadow-3xl flex justify-center items-center px-5 text-purple">
                    Dont worry, Ill check it
                  </div>
                  <span className="text-purple">12:38 AM</span>
                </div>
              </div>
            </div>
          </div>
          <div className="basis-1/2">
            <h3 className="text-2xl sm:text-3xl lg:text-4xl leading-normal lg:leading-relaxed font-bold text-[#554DDE] md:px-[50px]">
              Our support team Will be accountable as soon as possible 
            </h3>
            <p className="text-purple md:px-[50px] mt-[50px]">
              {" "}
              The first part of the tutorial: Lorem ipsum dolor sit amet,
              consectetur adipiscing elit, Lorem ipsum dolor sit amet.
            </p>
            <div className="mt-[50px] pl-[50px]">
              {" "}
              <Image
                className="mt-[50px]"
                src={avatars}
                alt=""
                layout="fixed"
                width={200}
              />
            </div>
          </div>
          <span className="absolute top-0 right-0 z-10">
            <Image src={circles} width={150} height={150} alt=""/>
          </span>
        </div>
      </div>
    </div>
  );
};

export default ContactSupport;
