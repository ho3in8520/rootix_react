import React from "react";
import Image from "next/image";
import mockupLeft from "../../../public/assets/images/mockup-left.png";
import mockupCenter from "../../../public/assets/images/mockup-center.png";
import mockupRight from "../../../public/assets/images/mockup-right.png";
import { Parallax } from "react-scroll-parallax";

function AboutMockups(props) {
  return (
    <div className="w-full">
      <div className="container mx-auto min-h-[650px] relative bottom-[0] z-[0]"> 
      <span className="hidden xl:block absolute bottom-[-50px] left-1/2 -translate-x-[90%] transform-gpu rotate-[-30deg] z-[1]">
        <Parallax rotate={["20", "-20"]}>
         
            <Image src={mockupLeft} alt="rootix app mockup" />
         
        </Parallax> 
        </span>
        <span className="absolute bottom-0 left-1/2 -translate-x-1/2 z-[2]">
          <Image src={mockupCenter} alt="rootix app mockup" />
        </span>
        
        <span className="hidden xl:block absolute bottom-[-50px] left-1/2 -translate-x-[10%]  transform-gpu rotate-[30deg] z-[1]">
        <Parallax rotate={["-20", "20"]}> <Image src={mockupRight} alt="rootix app mockup" /> </Parallax>
        </span>
      </div>
    </div>
  );
}

export default AboutMockups;
