import React from "react";
import Image from "next/image";
import aboutIcn01 from "../../../public/assets/images/about-1.png";
import aboutIcn02 from "../../../public/assets/images/about-2.png";
import aboutIcn03 from "../../../public/assets/images/about-3.png";
function AboutService(props) {
  return (
    <div className="bg-white dark:bg-navi-blue lg:py-[30px] py-[80px] ">
      <div className="container mx-auto">
        <h3 className="text-2xl sm:text-3xl lg:text-4xl leading-normal lg:leading-relaxed font-bold text-center">
          Rootix Services
        </h3>
        <div className="flex flex-col lg:flex-row items-center my-[30px] gap-x-[20px] lg:gap-x-[50px] gap-y-[40px]">
          <div className="basis-1/3 flex flex-col justify-center items-center w-[350px] lg:w-full shadow-3xl lg:shadow-none rounded p-5 ">
            <span>
              <Image src={aboutIcn01} alt="rootix" height={150}/>
            </span>
            <h3 className="px-[10px] xl:px-[30px] text-xl mt-[30px] leading-normal lg:leading-relaxed text-center font-semibold"> Lorem Ipsum is simply dummy text of the printing and typesetting</h3>
            <p className="mt-[40px] h-[100px] text-justify">  Lorem Ipsum is simply dummy text of the printing and typesetting
            industry. Lorem Ipsum has been the </p>
          </div>

          <div className="basis-1/3 flex flex-col justify-center items-center w-[350px] lg:w-full shadow-3xl lg:shadow-none rounded p-5">
            <span>
              <Image src={aboutIcn02} alt="rootix" height={150}/>
            </span>
            <h3 className="px-[10px] xl:px-[30px] text-xl mt-[30px] leading-normal lg:leading-relaxed text-center font-semibold"> Lorem Ipsum is simply dummy text of the printing and typesetting</h3>
            <p className="mt-[40px] h-[100px] text-justify">  Lorem Ipsum is simply dummy text of the printing and typesetting
            industry. Lorem Ipsum has been the industrys  Lorem Ipsum  </p>
          </div>
          <div className="basis-1/3 flex flex-col justify-center items-center w-[350px] lg:w-full shadow-3xl lg:shadow-none rounded p-5">
            <span>
              <Image src={aboutIcn03} alt="rootix" height={150}/>
            </span>
            <h3 className="px-[10px] xl:px-[30px] text-xl mt-[30px] leading-normal lg:leading-relaxed text-center font-semibold"> Lorem Ipsum is simply dummy text of the printing and typesetting</h3>
            <p className="mt-[40px] h-[100px] text-justify">  Lorem Ipsum is simply dummy text of the printing and typesetting
            industry. Lorem Ipsum has been the industrys  Lorem Ipsum  </p>
          </div>
        </div>
      </div>
    </div>
  );
}

export default AboutService;
