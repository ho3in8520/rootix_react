import React from "react";
import Image from "next/image";
import Link from "next/link";
import teamImg from "../../../public/assets/images/about.png";

function AboutTeam(props) {
  return (
    <div className="w-full pt-[200px] pb-[150px] lg:pb-[200px]">
      <div className="container mx-auto">
        <div className="flex flex-col lg:flex-row justify-between lg:gap-[30px] ">
          <div className="basis-1/2 justify-start flex flex-col">
            <div className="pb-[60px] border-b border-[#cccccc] flex flex-col">
              <h3 className="text-2xl sm:text-3xl lg:text-4xl leading-normal lg:leading-relaxed font-bold text-[#554DDE] text-center xl:text-left">
                Professional Team of Rootix
              </h3>

              <p className="text-purple mt-[50px] text-center xl:text-left">
                Lorem Ipsum is simply dummy text of the printing and typesetting
                industry. Lorem Ipsum has been the industrys standard dummy
                text ever since the 1500s, when an unknown printer took a galley
                of type and scrambled it to make a type specimen book.
              </p>
              <Link href="/login">
                <a className="mt-[50px] mx-auto lg:mx-0 px-4 py-4 rounded-md text-xl text-white bg-blue border-none rounded text-center transition duration-300 hover:bg-blue-dark w-[200px]">
                  Login
                </a>
              </Link>
            </div>
            <div className="pt-[60px] flex flex-col text-center mb-[50px] lg:mb-0">
                <p className="text-xl font-medium">Do you have any question? Contact Us</p>
                <Link href="tel:+4733378901">
                <a className="text-2xl font-semibold mt-[30px]">+996 (4343) 4325665</a>
                </Link>
            </div>
          </div>
          <div className="basis-1/2 flex justify-center">
            <Image src={teamImg} alt="Rootix Team" />
          </div>
        </div>
      </div>
    </div>
  );
}

export default AboutTeam;
