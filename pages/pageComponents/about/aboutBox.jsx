import React from "react";
import team from "../../../public/assets/images/about-box.png";
import Image from "next/image";
import Link from "next/link";

function AboutBox(props) {
  return (
    <div className="container mx-auto absolute right-0 left-0 top-[-100px]">
      <div className="flex flex-col-reverse lg:flex-row gap-y-[30px] w-full bg-white dark:bg-navi-blue-light shadow-3xl p-[50px] lg:p-[20px] xl:p-[50px] rounded justify-center items-center gap-x-[30px] xl:gap-x-0">
        <div className="basis-2/5">
          <Image src={team} alt="rootix team"/>
        </div>
        <div className="basis-3/5 flex flex-col">
          <span className="text-purple text-sm text-center lg:text-left">Rootix</span>
          <h3 className="text-2xl sm:text-3xl lg:text-4xl leading-normal lg:leading-relaxed font-bold text-grey-dark dark:text-white text-center lg:text-left py-[25px] lg:py-0">
            About Us
          </h3>
          <p>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
            eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim
            ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut
            aliquip ex ea commodo consequat. Duis aute irure dolor in
            reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla
            pariatur. Excepteur sint occaecat cupidatat non proident, sunt in
            culpa qui officia deserunt mollit anim id est laborum.
          </p>
        <Link href="/about-us">
        <a className="mt-[30px] px-4 py-4 rounded-md text-white bg-blue border-none rounded text-center transition duration-300 hover:bg-blue-dark w-[150px] mx-auto my-[20px] lg:mx-0">Contact Us</a>
        </Link>
        </div>
      </div>
    </div>
  );
}

export default AboutBox;
