import React from "react";
import { Swiper, SwiperSlide } from "swiper/react";
import { Autoplay } from "swiper";
import "swiper/css";
import "swiper/css/autoplay";

function AboutJoinUs(props) {
  return (
    <div className="pt-[750px] md:pt-[700px] lg:pt-[350px] pb-[100px] bg-navi-blue">
      <div className="container mx-auto">
        <div className="w-[80%] mx-auto flex flex-col justify-center items-center text-white mb-[50px]">
          <span className="text-purple text-sm">Rootix</span>
          <h3 className="text-2xl sm:text-3xl lg:text-4xl leading-normal lg:leading-relaxed font-bold mb-[30px] ">
            Our Commitments
          </h3>
          <p className="text-center">
            Lorem Ipsum is simply dummy text of the printing and typesetting
            industry. Lorem Ipsum has been the industrys standard dummy text
            ever since the 1500s, when an unknown printer took a galley of type
            and scrambled it to make a type specimen book.
          </p>
        </div>
        <div className="hidden lg:flex justify-center items-center gap-x-[20px]">
          <div className="basis-1/3 flex justify-start text-white relative pl-[60px]">
            <span className="text-[100px] text-purple absolute left-0 top-[-50px]">
              1
            </span>
            <div className="flex flex-col">
              <h3 className="text-2xl sm:text-xl lg:text-2xl leading-normal lg:leading-relaxed font-bold mb-[20px]">
                Security Of Wallets Somtething Belaa belaa
              </h3>
              <p className="text-justify">
                Lorem Ipsum is simply dummy text of the printing and typesetting
                industry. Lorem Ipsum has been the industrys standard dummy
                text ever since the 1500s, when an unknown printer took a galley
                of type and scrambled it to make a type specimen book.
              </p>
            </div>
          </div>
          <div className="basis-1/3 flex justify-start text-white relative pl-[60px]">
            <span className="text-[100px] text-purple absolute left-0 top-[-50px]">
              2
            </span>
            <div className="flex flex-col">
              <h3 className="text-2xl sm:text-xl lg:text-2xl leading-normal lg:leading-relaxed font-bold mb-[20px]">
                Security Of Wallets Somtething Belaa belaa
              </h3>
              <p className="text-justify">
                Lorem Ipsum is simply dummy text of the printing and typesetting
                industry. Lorem Ipsum has been the industrys standard dummy
                text ever since the 1500s, when an unknown printer took a galley
                of type and scrambled it to make a type specimen book.
              </p>
            </div>
          </div>

          <div className="basis-1/3 flex justify-start text-white relative pl-[60px]">
            <span className="text-[100px] text-purple absolute left-0 top-[-50px]">
              3
            </span>
            <div className="flex flex-col">
              <h3 className="text-2xl sm:text-xl lg:text-2xl leading-normal lg:leading-relaxed font-bold mb-[20px]">
                Security Of Wallets Somtething Belaa belaa
              </h3>
              <p className="text-justify">
                Lorem Ipsum is simply dummy text of the printing and typesetting
                industry. Lorem Ipsum has been the industrys standard dummy
                text ever since the 1500s, when an unknown printer took a galley
                of type and scrambled it to make a type specimen book.
              </p>
            </div>
          </div>
        </div>
        <div className="lg:hidden w-full px-0 md:px-[150px]">
            <JionUsSlider />
        </div>
      </div>
    </div>
  );
}

const JionUsSlider = () => {
  return (
    
    <Swiper
      modules={[Autoplay]}
      autoplay={{ delay: 5000 }}
      spaceBetween={50}
      slidesPerView={1}
    >
      <SwiperSlide>
        <div className="flex justify-start text-white relative pl-[60px]">
          <span className="text-[100px] text-purple absolute left-0 top-[-40px]">
            1
          </span>
          <div className="flex flex-col">
            <h3 className="text-2xl sm:text-xl lg:text-2xl leading-normal lg:leading-relaxed font-bold mb-[20px]">
              Security Of Wallets Somtething Belaa belaa
            </h3>
            <p className="text-justify">
              Lorem Ipsum is simply dummy text of the printing and typesetting
              industry. Lorem Ipsum has been the industrys standard dummy text
              ever since the 1500s, when an unknown printer took a galley of
              type and scrambled it to make a type specimen book.
            </p>
          </div>
        </div>
      </SwiperSlide>

      <SwiperSlide>
        <div className="flex justify-start text-white relative pl-[60px] ">
          <span className="text-[100px] text-purple absolute left-0 top-[-40px] pt-[10px]">
            2
          </span>
          <div className="flex flex-col">
            <h3 className="text-2xl sm:text-xl lg:text-2xl leading-normal lg:leading-relaxed font-bold mb-[20px]">
              Security Of Wallets Somtething Belaa belaa
            </h3>
            <p className="text-justify">
              Lorem Ipsum is simply dummy text of the printing and typesetting
              industry. Lorem Ipsum has been the industrys standard dummy text
              ever since the 1500s, when an unknown printer took a galley of
              type and scrambled it to make a type specimen book.
            </p>
          </div>
        </div>
      </SwiperSlide>

      <SwiperSlide>
        <div className="flex justify-start text-white relative pl-[60px]">
          <span className="text-[100px] text-purple absolute left-0 top-[-40px] pt-[10px]">
            3
          </span>
          <div className="flex flex-col">
            <h3 className="text-2xl sm:text-xl lg:text-2xl leading-normal lg:leading-relaxed font-bold mb-[20px]">
              Security Of Wallets Somtething Belaa belaa
            </h3>
            <p className="text-justify">
              Lorem Ipsum is simply dummy text of the printing and typesetting
              industry. Lorem Ipsum has been the industrys standard dummy text
              ever since the 1500s, when an unknown printer took a galley of
              type and scrambled it to make a type specimen book.
            </p>
          </div>
        </div>
      </SwiperSlide>
    </Swiper>
  );
};
export default AboutJoinUs;
