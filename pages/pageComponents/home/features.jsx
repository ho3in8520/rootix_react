import Image from 'next/image';
import feautre01 from "../../../public/assets/images/feature01.png";
import feautre02 from "../../../public/assets/images/feature02.png";
import feautre03 from "../../../public/assets/images/feature03.png";

import { Swiper, SwiperSlide } from "swiper/react";
import { Autoplay } from "swiper";
import "swiper/css";
import "swiper/css/autoplay";

const Features = ({}) => {
  return (
    <div className="w-full pt-[100px] relative bg-white dark:bg-navi-blue">
      <div className="container mx-auto flex flex-col justify-center items-center">
        <h3 className="text-2xl sm:text-3xl lg:text-4xl leading-normal lg:leading-relaxed font-bold">Get started in no time</h3>
        <p className="text-center text-purple xl:w-4/12 text-base mt-[50px]">
          The first part of the tutorial: Lorem ipsum dolor sit amet,
          consectetur adipiscing elit
        </p>

        <div className="hidden md:flex justify-between w-full xl:px-[100px] mt-[50px] pb-[50px] z-10 gap-x-[20px] lg:gap-x-[50px]">
          <FeatureCard
            img={feautre01}
            title="Registeration"
            dsc="The first part of the tutorial: Lorem ipsum dolor sit amet, consectetur adipiscing elit"
          />
          <FeatureCard
            img={feautre02}
            title="Trade"
            dsc="The first part of the tutorial: Lorem ipsum dolor sit amet, consectetur adipiscing elit"
          />
          <FeatureCard
            img={feautre03}
            title="Personal Wallet"
            dsc="The first part of the tutorial: Lorem ipsum dolor sit amet, consectetur adipiscing elit"
          />
        </div>

        <div className='flex justify-center mt-[50px] pb-[50px] z-10 md:hidden w-full'>
          <FeaturesSlider />
        </div>
      </div>
      <span
        className="bg-grey-light w-full h-[300px] absolute right-0 left-0 bottom-0 dark:bg-transparent"></span>
    </div>
  );
};

export default Features;


const FeatureCard = ({img, title, dsc}) => {
  return (
      <div className="w-[300px] mx-auto md:w-4/12 min-h-[300px] bg-white rounded-lg border border-[#e3e3e3] flex flex-col justify-between items-center py-5 px-[20px] dark:bg-navi-blue-light dark:border-none">
          <div className='mb-[10px]'>
              <Image src={img} alt="" height={140}/>
          </div>
          <h4 className='font-semibold text-lg'>{title}</h4>
          <p className='text-center'>{dsc}</p>
      </div>
  );
};


const FeaturesSlider = () =>{
  return (
    
       <Swiper
        modules={[Autoplay]}
        autoplay={{ delay: 500000 }}
        spaceBetween={50}
        slidesPerView={1}
      >
        <SwiperSlide>
        <FeatureCard
            img={feautre01}
            title="Registeration"
            dsc="The first part of the tutorial: Lorem ipsum dolor sit amet, consectetur adipiscing elit"
          />
        </SwiperSlide>
        <SwiperSlide>
        <FeatureCard
            img={feautre01}
            title="Registeration"
            dsc="The first part of the tutorial: Lorem ipsum dolor sit amet, consectetur adipiscing elit"
          />
        </SwiperSlide>
        <SwiperSlide>
        <FeatureCard
            img={feautre01}
            title="Registeration"
            dsc="The first part of the tutorial: Lorem ipsum dolor sit amet, consectetur adipiscing elit"
          />
        </SwiperSlide>
        </Swiper>
  )
}