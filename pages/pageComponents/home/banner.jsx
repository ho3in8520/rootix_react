import Link from "next/link";
import style from "../../../styles/pages/home.module.css";
import cover from "../../../public/assets/images/cover.jpg";
import Image from "next/image";
import { PlayIcon } from "@heroicons/react/solid";
import hand from "../../../public/assets/images/btc-hand.png";
const Banner = () => {


  return (
    <div className={`w-full ${style.banner} pb-[200px]`}>
 
      <div className="container pt-[190px] flex flex-row mx-auto pb-10">
        
        <div className="h-full xl:basis-1/2 bg-[url('../public/assets/images/hero-bg.svg')] bg-center	bg-no-repeat bg-contain xl:bg-none">
          <h1 className="text-2xl sm:text-3xl lg:text-5xl leading-normal lg:leading-relaxed font-semibold text-white h-full">
            Easy and safe way to get <br />
            Crypto currencis in <br />
            <span className="text-orange"> Rootix </span>
          </h1>

          <p className="text-purple mt-[35px]">
            Experience high security with us, have your own wallet. Trade with
            the lowest commission, a variety of high-speed trades only on Rootix
          </p>

          <div className="flex flex-row mt-[35px]">
            <Link href="/register">
              <a
                className={`px-4 py-2 text-white bg-orange border-none rounded min-w-[150px] text-center transition duration-300 hover:bg-orange-dark`}
              >
                register
              </a>
            </Link>

            <Link href="/about-us">
              <a
                className="px-4 py-2 text-orange rounded border border-orange mx-3 min-w-[150px]
                 text-center transition duration-300 hover:bg-orange-dark hover:text-white"
              >
                About Us
              </a>
            </Link>
          </div>

          <div className="flex flex-row mt-[35px]">
            <div className="basis-1/3 relative">
              <Image className="rounded" src={cover} alt="" />
              <span className="rounded absolute right-0 left-0 top-0 bottom-0 w-full h-full bg-[#00000080] cursor-pointer flex justify-center items-center">
                <PlayIcon className="w-[35px] text-white opacity-50 hover:opacity-100" />
              </span>
            </div>
            <div className="basis-2/3 flex flex-col justify-between ml-5 text-purple py-4">
              <p>
                The first part of the tutorial: Lorem ipsum dolor sit
                <br />
                amet, consectetur adipiscing elit
              </p>
              <p className="cursor-pointer transition duration-300 hover:text-orange">
                Watch preview
              </p>
            </div>
          </div>
        </div>
        <div className="basis-1/2 hidden xl:block bg-[url('../public/assets/images/hero-bg.svg')] bg-cover xl:relative absolute">
          <span className="absolute top-[150px] animate-wiggle">
            <Image src={hand} width="80" height="80" alt="" />
          </span>
          <span className="absolute top-[70px] right-[160px] transform -translate-x-1/2 animate-wiggle">
            <Image src={hand} width="80" height="80" alt="" />
          </span>
        </div>
      </div>
    </div>
  );
};

export default Banner;
