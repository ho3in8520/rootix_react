import Image from "next/image";
import partner01 from "../../../public/assets/images/partner01.png";
import partner02 from "../../../public/assets/images/partner02.png";
import partner03 from "../../../public/assets/images/partner03.png";
import partner04 from "../../../public/assets/images/partner04.png";
import partner05 from "../../../public/assets/images/partner05.png";
import partner06 from "../../../public/assets/images/partner06.png";

import { Swiper, SwiperSlide } from "swiper/react";
import { Autoplay } from "swiper";
import "swiper/css";
import "swiper/css/autoplay";

const Partners = ({}) => {
  return (
    <div className="w-full py-[100px] border-b-2 border-[#E2E2FB] dark:border-none bg-white dark:bg-navi-blue">
      <div className="container mx-auto flex flex-col justify-center items-center">
        <h3 className="text-2xl sm:text-3xl lg:text-4xl leading-normal lg:leading-relaxed font-bold">Our Partners</h3>
        <p className="text-center text-purple lg:w-4/12 text-base mt-[50px]">
          The first part of the tutorial: Lorem ipsum dolor sit amet,
          consectetur adipiscing elit
        </p>
        <div className="lg:flex w-full mt-[50px] justify-center hidden">
          <div className="basis-1/3 md:basis-1/6 flex justify-center">
            <Image src={partner01} alt="" layout="intrinsic" width={140} />
          </div>
          <div className="basis-1/3 md:basis-1/6 flex justify-center">
            <Image src={partner02} alt="" layout="intrinsic" width={140} />
          </div>
          <div className="basis-1/3 md:basis-1/6 flex justify-center">
            <Image src={partner03} alt="" layout="intrinsic" width={140} />
          </div>
          <div className="basis-1/3 md:basis-1/6 flex justify-center">
            <Image src={partner04} alt="" layout="intrinsic" width={140} />
          </div>
          <div className="basis-1/3 md:basis-1/6 flex justify-center">
            <Image src={partner05} alt="" layout="intrinsic" width={140} />
          </div>
          <div className="basis-1/3 md:basis-1/6 flex justify-center">
            <Image src={partner06} alt="" layout="intrinsic" width={140} />
          </div>
        </div>

        <PartnersSlider />
      </div>
    </div>
  );
};

export default Partners;

const PartnersSlider = () => {
  return (
    <div className="lg:hidden w-full mt-[50px]">
      <Swiper
        modules={[Autoplay]}
        autoplay={{ delay: 5000 }}
        spaceBetween={50}
        slidesPerView={3}
      >
        <SwiperSlide>
        <div className="">
          <Image src={partner01} alt="" width={140} />
        </div>
        </SwiperSlide>

        <SwiperSlide>
        <div className="">
          <Image src={partner02} alt="" width={140} />
        </div>
        </SwiperSlide>

        <SwiperSlide>
        <div className="">
          <Image src={partner03} alt="" width={140} />
        </div>
        </SwiperSlide>

        <SwiperSlide>
        <div className="">
          <Image src={partner04} alt="" width={140} />
        </div>
        </SwiperSlide>

        <SwiperSlide>
        <div className="">
          <Image src={partner05} alt="" width={140} />
        </div>
        </SwiperSlide>

        <SwiperSlide>
        <div className="">
          <Image src={partner06} alt="" width={140} />
        </div>
        </SwiperSlide>

      </Swiper>
    </div>
  );
};
