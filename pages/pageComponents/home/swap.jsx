import Image from "next/image";
import { Parallax } from "react-scroll-parallax";
import Select from "react-select";
import Link from "next/link";
import { useTheme } from "next-themes";
import panelSc from "../../../public/assets/images/panelScreenshot.png";
import BarChart from "../../../components/common/barChart";
import {amountFormatter} from "../../../utils/amountFormatter.js";
import { useState } from "react";


const Swap = ({ data }) => {
  const { resolvedTheme: theme } = useTheme();
  const swapCoins = data ? data?.coins?.swap_coins : [];

  const [sendPrice, setSentPrice] = useState(0);
  const [sendAmount, setSendAmount] = useState(0);
  
  const [receivePrice, setReceivePrice] = useState(0);

  const selectStyles = {
    control: (provided, state) => ({
      ...provided,
      border: "0px",
      height: "70px",
      boxShadow:
        "rgba(14, 30, 37, 0.12) 0px 2px 4px 0px, rgba(14, 30, 37, 0.32) 0px 2px 16px 0px",
      borderRadius: "10px",
      paddingLeft: "10px",
      background: "transparent",
    }),
    menu: (provided, state) => ({
      ...provided,
      background: theme === "dark" ? "#2b3053" : "white",
    }),
    option: (provided, state) => ({
      ...provided,
      background: "transparent",
      color: theme === "dark" ? "white" : "#666666",
    }),
  };

  const calcReceiveAmount = () =>{
    console.log("sendPrice:", sendPrice, "sendAmount:", sendAmount, "receivePrice:", receivePrice);
    const amount = sendPrice * sendAmount / receivePrice
    return amountFormatter(amount) || 0;
   
  }

  return (
    <>
      <div className="bg-[url('../public/assets/images/swapBg.png')] dark:bg-[url('../public/assets/images/swapBgDark.png')] bg-right bg-no-repeat w-full bg-white dark:bg-navi-blue pt-[500px] md:pt-[200px]">
        <div className="flex container min-h-[500px] pt-[200px] mx-auto items-center">
          <div className="h-full basis-1/2 hidden lg:block">
            <div className="flex w-full justify-around mb-[30px] relative h-[200px]">
              <Parallax translateY={["50px", "-50px"]}>
                <div className="min-w-[250px] min-h-[150px] bg-white shadow-xl rounded-xl p-5  left-[30px] dark:bg-navi-blue-light">
                  <h3 className="text-[18px]">Total Market Cap</h3>
                  <div className="flex gap-x-3">
                    <p className="font-semibold text-[20px]">
                      $1,249,840,870,459
                    </p>
                    <p className="text-[#00E1F0] dark:text-[#00E1F0]">%14</p>
                  </div>
                  <div className="m-x-auto">
                    <BarChart data={[9, 3, 5, 7, 8, 9]} bg={"green"} />
                  </div>
                </div>
              </Parallax>

              <Parallax translateY={["-50px", "50px"]}>
                <div className="min-w-[250px] min-h-[150px] bg-white shadow-xl rounded-xl p-5 dark:bg-navi-blue-light">
                  <h3 className="text-[18px]">24h Vol</h3>
                  <div className="flex gap-x-3">
                    <p className="font-semibold text-[20px]">$71,205,396,605</p>
                    <p className="text-[#cc0000] dark:text-[#cc0000]">%5</p>
                  </div>
                  <div className="m-x-auto">
                    <BarChart data={[9, 5, 6, 5, 3, 2]} bg={"red"} />
                  </div>
                </div>
              </Parallax>
            </div>
            <div className="shadow-2xl">
              <Image src={panelSc} alt="" quality={100} />
            </div>
          </div>

          <div className="lg:basis-1/2 h-full flex flex-col  px-[50px]">
            <h3 className="text-2xl sm:text-3xl lg:text-4xl leading-normal lg:leading-relaxed font-bold	">
              At Rootix Secure Exchange Buy and sell
            </h3>
            <p className="my-[50px] text-purple text-lg">
              The first part of the tutorial: Lorem ipsum dolor sit amet,
              consectetur adipiscing elit
            </p>
            <div className="flex w-full justify-between">
              <div className="basis-1/3 text-white">
                <Select
                  className="bg-white dark:bg-navi-blue-light rounded-xl"
                  options={swapCoins}
                  styles={selectStyles}
                  defaultValue={swapCoins[0]}
                  getOptionLabel={(e) => {
                    setSentPrice(e.price);
                    return (
                      <div style={{ display: "flex", alignItems: "center" }}>
                        {e.logo_url && (
                          <Image
                            className="rounded-full"
                            src={e.logo_url}
                            alt={e.name}
                            width={25}
                            height={25}
                          />
                        )}
                        <span className="text-grey-dark dark:text-white ml-2">
                          {e.currency}
                        </span>
                      </div>
                    );
                  }}
                />
              </div>

              <div className="basis-2/3 flex jsutify-center items-center ml-2 shadow-3xl rounded-md	px-4 bg-white dark:bg-navi-blue-light dark:text-white">
                <span className="text-center px-3 bg-white dark:bg-navi-blue-light h-full flex items-center font-medium text-base md:text-xl dark:text-white ">
                  send
                </span>
                <input
                  className="w-full border-none h-full rounded focus:outline-0 rounded-md	font-medium	text-right text-base md:text-xl px-5 bg-white dark:bg-navi-blue-light dark:text-white"
                  type="number"
                  min={0}
                  onChange={(e) => setSendAmount(e.target.value)}
                />
              </div>
            </div>

            <div className="flex w-full justify-between mt-[40px]">
              <div className="basis-1/3">
                <Select
                  className="bg-white dark:bg-navi-blue-light rounded-xl"
                  options={swapCoins}
                  styles={selectStyles}
                  defaultValue={swapCoins[2]}
                  getOptionLabel={(e) => {
                    setReceivePrice(e.price);
                    return (
                      <div style={{ display: "flex", alignItems: "center" }}>
                        {e.logo_url && (
                          <Image
                            className="rounded-full"
                            src={e.logo_url}
                            alt={e.name}
                            width={25}
                            height={25}
                          />
                        )}
                        <span className="text-grey-dark dark:text-white ml-2">
                          {e.currency}
                        </span>
                      </div>
                    );
                  }}
                />
              </div>

              <div className="basis-2/3 flex jsutify-center items-center ml-2 shadow-3xl rounded-md	px-4 bg-white dark:bg-navi-blue-light dark:text-black">
                <span className="text-center px-3 bg-white dark:bg-navi-blue-light h-full flex items-center font-medium text-base md:text-xl dark:text-white ">
                  receive
                </span>
                <div
                  className="w-full border-none h-full rounded focus:outline-0 rounded-md	font-medium
                  	text-right text-base md:text-xl px-5 bg-white dark:bg-navi-blue-light dark:text-white
                    flex items-center flex-row-reverse"
                >
                  {calcReceiveAmount()}
                </div>
              </div>
            </div>

            <Link href="/login">
              <a className="mt-[50px] px-4 py-4 rounded-md text-xl text-white bg-blue border-none rounded w-full text-center transition duration-300 hover:bg-blue-dark ">
                Swap
              </a>
            </Link>
          </div>
        </div>
      </div>
    </>
  );
};
export default Swap;
