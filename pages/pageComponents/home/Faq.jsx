import React from "react";
import Link from "next/link";
import { ChevronRightIcon } from "@heroicons/react/solid";
import FaqAccordion from "../../../components/common/FaqAccordion";


function Faq(props) {
  return (
    <div id="main-page-faq" className="pt-[50px] relative bg-white dark:bg-navi-blue">
      <span className="w-[400px] h-[400px] rounded-full bg-[#F5F8FF] dark:bg-navi-blue-light absolute left-[-50px] top-0"></span>
      <div className="container mx-auto relative pb-[105px]">
        <div className="flex flex-col lg:flex-row">
          <div className="basis-1/2 flex flex-col items-between pr-[40px]">
            <h3 className="font-bold text-2xl sm:text-3xl lg:text-4xl leading-normal lg:leading-relaxed">
              Do you have any question?
              <br />
              Read on FAQ
            </h3>
            <p className="my-[50px] text-purple">
              The first part of the tutorial: Lorem ipsum dolor sit amet,
              consectetur adipiscing elitThe first part of the tutorial: Lorem
              ipsum dolor sit amet, consectetur adipiscing elit
            </p>
            <Link href="#">
              <a className="lg:flex items-center text-blue font-semibold w-fit ml-auto hover:text-blue-dark transition hidden">
                More Questions <ChevronRightIcon width={20} />
              </a>
            </Link>
          </div>
          <div className="basis-1/2">
          

            <>
            <FaqAccordion num={1} question={"How Rootix works ?"} >
                The first part of the tutorial: Lorem ipsum dolor sit amet,
                consectetur adipiscing elitThe first part of the tutorial: Lorem
                ipsum dolor sit amet, consectetur adipiscing elit
              </FaqAccordion>
              <FaqAccordion num={2} question={"How much are Rootix fees ?"}>
                The first part of the tutorial: Lorem ipsum dolor sit amet,
                consectetur adipiscing elitThe first part of the tutorial: Lorem
                ipsum dolor sit amet, consectetur adipiscing elit
              </FaqAccordion>

              <FaqAccordion num={3} question={"Does Rootix have a mobile app?"}>
                The first part of the tutorial: Lorem ipsum dolor sit amet,
                consectetur adipiscing elitThe first part of the tutorial: Lorem
                ipsum dolor sit amet, consectetur adipiscing elit
              </FaqAccordion>

              <FaqAccordion num={4} question={"What features does Rootix offer?"}>
                The first part of the tutorial: Lorem ipsum dolor sit amet,
                consectetur adipiscing elitThe first part of the tutorial: Lorem
                ipsum dolor sit amet, consectetur adipiscing elit
              </FaqAccordion> 
            </>

            <Link href="#">
              <a className="block lg:hidden px-4 py-2 text-white bg-orange border-none rounded min-w-[150px] w-fit text-center transition duration-300 hover:bg-orange-dark mx-auto mt-[50px]">
                More Questions
              </a>
            </Link>
          </div>
        </div>
        <span className="w-[100px] h-[100px] absolute right-0 bottom-0 rounded-br-full bg-[#CCF9FC] dark:bg-[#093D5C] hidden lg:block"></span>
      </div>
    </div>
  );
}

export default Faq;
