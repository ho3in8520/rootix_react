import fireIcon from "../../../public/assets/icons/fire.png";
import rocketIcon from "../../../public/assets/icons/rocket.png";
import diamondIcon from "../../../public/assets/icons/diamond.png";
import Image from "next/image";
import Link from "next/link";
import { ChevronRightIcon } from "@heroicons/react/solid";
import _ from "lodash";
import LineChart from "../../../components/common/lineChart";
import { Parallax } from "react-scroll-parallax";
import {priceFormatter} from "../../../utils/priceFormatter";

const Coins = ({ data }) => {
  const generateRandomData = () => {
    const chartData = Array.from({ length: 10 }, () =>
      Math.floor(Math.random() * 50)
    );
    return chartData;
  };


  const trendingCoins = (data?.status === 200) ? Object?.values(data?.coins?.top_currencies) : []
  const recentlyAdded = (data?.status === 200) ? Object?.values(data?.coins?.recently) : []



  if (data?.status === 200)
    return (
      <Parallax translateY={["-100px", "100px"]}>
        <div
          className="container right-0 py-6 left-0 mx-auto bg-white rounded-2xl flex flex-col md:flex-row absolute
       top-[-250px] shadow-xl min-h-[300px] z-20 bg-white dark:bg-navi-blue-light"
        >
          <div className="basis-1/3 flex flex-col md:border-r-2 px-4 border-grey pb-[20px] md:pb-0 border-b md:border-b-0">
            <div className="w-full flex items-center">
              <Image className="w-[80px]" src={fireIcon} alt="" />
              <h2 className="grow text-black font-medium dark:text-white uppercase text-sm lg:text-base">
                TRENDING
              </h2>
              <Link href="/prices">
                <a className="text-blue flex justify-center items-center font-medium text-sm lg:text-base">
                  MORE
                  <ChevronRightIcon className="w-[20px]" />
                </a>
              </Link>
            </div>

            <div className="mt-3">
              <div className="flex px-2">
                <p className="basis-1/2 xl:basis-1/3 font-medium">Name</p>
                <p className="basis-1/2 xl:basis-1/3 font-medium">Price</p>
                <p className="basis-1/3 font-medium hidden xl:block">Chart</p>
              </div>
              {trendingCoins?.map((coin) => (
                <div key={coin.price} className="flex flex-col mt-4 px-2">
                  <div className="flex items-center">
                    <div className="flex basis-1/2 xl:basis-1/3 items-center gap-x-1">
                      <Image
                        src={coin.logo_url}
                        alt=""
                        width={20}
                        height={20}
                      />
                      <Link href="#">
                        <a className="hover:text-orange">{coin.currency}</a>
                      </Link>
                    </div>

                    <div className="flex basis-1/2 xl:basis-1/3 items-center gap-x-1">
                      <span
                        className={
                          coin._1d >= 0 ? "text-[#2eb82e]" : "text-[#e60000]"
                        }
                      >
                       {priceFormatter(coin.price)}
                      </span>
                    </div>

                    <div className="flex basis-1/3 items-center gap-x-1  hidden xl:block">
                      <LineChart
                        data={generateRandomData()}
                        trend={coin._1d >= 0 ? "bullish" : "bearish"}
                      />
                    </div>
                  </div>
                </div>
              ))}
            </div>
          </div>

          <div className="basis-1/3 flex flex-col md:border-r-2 px-4 border-grey py-[20px] md:py-0 border-b md:border-b-0">
            <div className="w-full flex items-center gap-x-1">
              <Image className="w-[80px]" src={rocketIcon} alt="" />
              <h2 className="grow text-black text-sm lg:text-base lg:font-medium dark:text-white uppercase">
                top gainers
              </h2>
              <Link href="#">
                <a className="text-blue flex justify-center items-center font-medium text-sm lg:text-base">
                  MORE
                  <ChevronRightIcon className="w-[20px]" />
                </a>
              </Link>
            </div>
            <div className="mt-3">
              <div className="flex px-2">
                <p className="basis-1/2 xl:basis-1/3 font-medium">Name</p>
                <p className="basis-1/2 xl:basis-1/3 font-medium">Price</p>
                <p className="basis-1/3 font-medium hidden xl:block">Chart</p>
              </div>
              {trendingCoins?.map((coin) => (
                <div key={coin.price} className="flex flex-col mt-4 px-2">
                  <div className="flex items-center">
                    <div className="flex basis-1/2 xl:basis-1/3 items-center gap-x-1">
                      <Image
                        src={coin.logo_url}
                        alt=""
                        width={20}
                        height={20}
                      />
                      <Link href="#">
                        <a className="hover:text-orange">{coin.currency}</a>
                      </Link>
                    </div>

                    <div className="flex basis-1/2 xl:basis-1/3 items-center gap-x-1">
                      <span
                        className={
                          coin._1d >= 0 ? "text-[#2eb82e]" : "text-[#e60000]"
                        }
                      >
                         {priceFormatter(coin.price)}
                      </span>
                    </div>

                    <div className="flex basis-1/3 items-center gap-x-1  hidden xl:block">
                      <LineChart
                        data={generateRandomData()}
                        trend={coin._1d >= 0 ? "bullish" : "bearish"}
                      />
                    </div>
                  </div>
                </div>
              ))}
            </div>
          </div>

          <div className="basis-1/3 flex flex-col px-4 pt-[20px] md:pt-0">
            <div className="w-full flex items-center gap-x-1">
              <Image className="w-[80px]" src={diamondIcon} alt="" />
              <h2 className="grow text-black font-medium dark:text-white uppercase text-xs lg:text-base">
                RECENTLY ADDED
              </h2>
              <Link href="prices">
                <a className="text-blue flex justify-center items-center font-medium text-sm lg:text-base">
                  MORE
                  <ChevronRightIcon className="w-[20px]" />
                </a>
              </Link>
            </div>

            <div className="mt-3">
              <div className="flex px-2">
                <p className="basis-1/2 xl:basis-1/3 font-medium">Name</p>
                <p className="basis-1/2 xl:basis-1/3 font-medium">Price</p>
                <p className="basis-1/3 font-medium hidden xl:block">Chart</p>
              </div>

              {recentlyAdded?.map((coin) => (
                <div key={coin.price} className="flex flex-col mt-4 px-2">
                  <div className="flex items-center">
                    <div className="flex basis-1/2 xl:basis-1/3 items-center gap-x-1">
                      <Image
                        src={coin.logo_url}
                        alt=""
                        width={20}
                        height={20}
                      />
                      <Link href="/prices">
                        <a className="hover:text-orange">{coin.currency}</a>
                      </Link>
                    </div>

                    <div className="flex basis-1/2 xl:basis-1/3 items-center gap-x-1">
                      <span
                        className={
                          coin._1d >= 0 ? "text-[#2eb82e]" : "text-[#e60000]"
                        }
                      >
                         {priceFormatter(coin.price)}
                      </span>
                    </div>

                    <div className="flex basis-1/3 items-center gap-x-1 hidden xl:block">
                      <LineChart
                        data={generateRandomData()}
                        trend={coin._1d >= 0 ? "bullish" : "bearish"}
                      />
                    </div>
                  </div>
                </div>
              ))}
            </div>
          </div>
        </div>
      </Parallax>
    );
  else
    return (
      <Parallax translateY={["-100px", "100px"]}>
        <div className="container right-0 py-6 left-0 mx-auto bg-white rounded-2xl flex flex-col items-center md:flex-row absolute top-[-250px] shadow-xl  min-h-[300px] z-20">
          <h2 className="mx-auto text-[#e10000]">{data?.message}</h2>
        </div>
      </Parallax>
    );
};

export default Coins;
