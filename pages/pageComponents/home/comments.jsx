import React from "react";
import { StarIcon } from "@heroicons/react/solid";
import Image from "next/image";
import avatar from "../../../public/assets/images/avatar.png";
import style from "../../../styles/pages/home.module.css";

import { Swiper, SwiperSlide } from "swiper/react";
import { Autoplay, Pagination } from "swiper";
import "swiper/css";
import "swiper/css/autoplay";
import "swiper/css/pagination";

function Comments(props) {
  return (
    <div className="pt-[150px] bg-[url('../public/assets/images/swapBg.png')] dark:bg-[url('../public/assets/images/swapBgDark.png')] bg-left bg-no-repeat bg-white dark:bg-navi-blue">
      <div className="container mx-auto text-center">
        <span className="text-blue mx-auto uppercase font-bold ">comments</span>
        <h3 className="py-[30px] lg:py-[10px] font-bold text-center text-2xl sm:text-3xl lg:text-4xl leading-normal lg:leading-relaxed	">
          What do our users say about
          <br /> Rootix?
        </h3>
        <div className="mb-[30px]">
          <Swiper
            modules={[Autoplay, Pagination]}
            autoplay={{ delay: 5000 }}
            spaceBetween={40}
            pagination={{ clickable: true }}
            slidesPerView={1}
            breakpoints={
             { 0: {
                slidesPerView: 1,
                spaceBetween: 10,
              },
              760: {
                slidesPerView: 2,
                spaceBetween: 20,
              },
              960: {
                slidesPerView: 3,
                spaceBetween: 40,
              },}
          }
          >
            <SwiperSlide>
              <Slide name={"Hamed Izadi"} role={"User"} avatar={avatar}>
                The first part of the tutorial: Lorem ipsum dolor sit amet,
                consectetur adipiscing elit
              </Slide>
            </SwiperSlide>
            <SwiperSlide>
              <Slide name={"Hamed Izadi"} role={"User"} avatar={avatar}>
                The first part of the tutorial: Lorem ipsum dolor sit amet,
                consectetur adipiscing elit
              </Slide>
            </SwiperSlide>
            <SwiperSlide>
              <Slide name={"Hamed Izadi"} role={"User"} avatar={avatar}>
                The first part of the tutorial: Lorem ipsum dolor sit amet,
                consectetur adipiscing elit
              </Slide>
            </SwiperSlide>
            <SwiperSlide>
              <Slide name={"Hamed Izadi"} role={"User"} avatar={avatar}>
                The first part of the tutorial: Lorem ipsum dolor sit amet,
                consectetur adipiscing elit
              </Slide>
            </SwiperSlide>
            <SwiperSlide>
              <Slide name={"Hamed Izadi"} role={"User"} avatar={avatar}>
                The first part of the tutorial: Lorem ipsum dolor sit amet,
                consectetur adipiscing elit
              </Slide>
            </SwiperSlide>
          </Swiper>
        </div>
      </div>
    </div>
  );
}

export default Comments;

const Slide = ({ children, name, role, avatar }) => {
  return (
    <div
      className={`pt-[30px] px-[30px] pb-[20px] mb-[70px] border bg-white border-grey dark:border-none dark:bg-navi-blue-light static h-[300px] my-[27px] flex flex-col ${style.slideCartShadow}`}
    >
      <span className="w-[55px] h-[55px] rounded-full absolute top-[0px] left-[50px] bg-blue text-white text-4xl flex leading-relaxed justify-center">
        ،،
      </span>
      <div className="flex mt-[40px]">
        <StarIcon width={20} color="#FE8B75" />
        <StarIcon width={20} color="#FE8B75" />
        <StarIcon width={20} color="#FE8B75" />
        <StarIcon width={20} color="#FE8B75" />
        <StarIcon width={20} color="grey" />
      </div>

      <p className="flex h-[150px] py-[20px]">{children}</p>
      <div className="border-t border-grey flex justify-start items-center pt-[10px]">
        <Image src={avatar} alt="#" className="rounded-full" />
        <div className="flex flex-col justify-start items-start mx-4">
          <p className="">{name}</p>
          <p className="text-purple text-sm">{role}</p>
        </div>
      </div>
    </div>
  );
};
