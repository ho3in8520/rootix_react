import React from "react";
import Link from "next/link";
import Image from "next/image";
import { ArrowNarrowRightIcon } from "@heroicons/react/solid";
import { motion } from "framer-motion";

const LatestPosts = ({ data }) => {
  return (
    <div className="w-full pt-[100px] bg-white dark:bg-navi-blue">
      <div className="container mx-auto">
        <div className="w-full mb-[50px] flex flex-row">
          <div className="basis-5/6">
            <h3 className="font-bold text-4xl">Latest Rootixs Blog Articles</h3>
          </div>
          <div className="basis-1/6 rtl">
            <Link href="#">
              <a
                href="#"
                className="flex gap-x-2 text-blue hover:text-blue-dark transition"
              >
                <ArrowNarrowRightIcon width={25} color="#3734A9" />
                Enter Blog
              </a>
            </Link>
          </div>
        </div>

        <div className="flex gap-x-8 w-full">
          {data?.slice(0, 2).map((post) => {
            return (
              <div key={post.id} className="basis-1/3">
                <div>
                  <a href={post.link} target="_blank" rel="noreferrer">
                    <motion.div
                      whileHover={{
                        scale: 1.02,
                        transition: { duration: 0.1 },
                      }}
                      whileTap={{ scale: 0.98 }}
                    >
                      <Image
                        className="rounded-2xl cursor-pointer"
                        src={post.thumbnail_url}
                        alt={post.title.rendered}
                        layout="responsive"
                        height={270}
                        width={320}
                      />
                    </motion.div>
                  </a>

                  <p className="text-purple text-sm my-4">
                    {post.date.split("").slice(0, 10).join("")}
                  </p>
                  <div className="h-[50px]">
                    <a
                      href={post.link}
                      target="_blank" rel="noreferrer"
                      className="font-bold text-lg hover:text-purple transition"
                    >
                      {post.title.rendered}
                    </a>
                  </div>

                  <p className="my-4 text-justify	">{post.excerpt.rendered}</p>
                </div>
              </div>
            );
          })}

          <div className="basis-1/3 flex flex-col divide-y-2 divide-grey">
            {data?.slice(3, 7).map((post) => {
              return (
                <div className="flex flex-row py-5" key={post.id}>
                  <div className="mr-5">
                    <motion.div
                      whileHover={{
                        scale: 1.02,
                        transition: { duration: 0.1 },
                      }}
                      whileTap={{ scale: 0.98 }}
                    >
                      <Image
                        className="rounded-xl cursor-pointer"
                        src={post.thumbnail_url}
                        alt={post.title.rendered}
                        layout="fixed"
                        height={100}
                        width={100}
                      />
                    </motion.div>
                  </div>
                  <div className=" flex flex-col">
                    <p className="text-purple text-sm my-4">
                      {post.date.split("").slice(0, 10).join("")}
                    </p>
                    <a
                      href={post.link}
                      className="font-semibold hover:text-purple"
                    >
                      {post.title.rendered}
                    </a>
                  </div>
                </div>
              );
            })}
          </div>
        </div>
      </div>
    </div>
  );
};

export default LatestPosts;
