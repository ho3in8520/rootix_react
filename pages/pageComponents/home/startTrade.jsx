import React from "react";
import { ArrowNarrowRightIcon } from "@heroicons/react/solid";
import Image from "next/image";
import { Parallax } from "react-scroll-parallax";
import avatars from "../../../public/assets/images/avatars.png";
import curvyArrow from "../../../public/assets/images/curvyArrow.png";
import startTrade from "../../../public/assets/images/startTrade.png";
import linkdinIcn from "../../../public/assets/icons/linkedin.png";
import youtubeIcn from "../../../public/assets/icons/youtube.png";
import instaIcn from "../../../public/assets/icons/insta.png";
import twitterIcn from "../../../public/assets/icons/twitter.png";
import rootix from "../../../public/assets/images/rootix.png";

import Link from "next/link";

function StartTrade(props) {
  return (
    <div className=" w-full py-[100px] bg-white dark:bg-navi-blue">
      <div className="container mx-auto flex flex-col justify-center items-center lg:flex-row">
        <div className="basis-5/12 flex flex-col relative">
          <h3 className="text-2xl sm:text-3xl lg:text-4xl leading-normal lg:leading-relaxed font-bold text-center lg:text-left">
            Professional And Fast
            <br className="hidden lg:block"/> User Panel, Solution <br className="hidden lg:block"/>
            For More Profit
          </h3>
          <p className="mt-[50px] text-purple text-justify leading-7">
            The first part of the tutorial: Lorem ipsum dolor sit amet,
            consectetur adipiscing elitThe first part of the tutorial: Lorem
            ipsum dolor sit amet, consectetur adipiscing elitThe first part of
            the tutorial: Lorem ipsum dolor sit amet, consectetur adipiscing
            elit
          </p>

          <span className="absolute right-[-50px] bottom-[150px] hidden lg:block">
            <Parallax translateX={["-50px", "50px"]}>
              <Image src={curvyArrow} alt=""/>
            </Parallax>
          </span>
          <Link href="#">
            <a
              className="cursor-pointer mt-[50px] px-3 py-3 rounded-md text-xl 
          text-white bg-blue border-none rounded w-full text-center transition
           duration-300 hover:bg-blue-dark w-[170px] flex justify-center"
            >
              START
              <ArrowNarrowRightIcon width={20} className="ml-3" />
            </a>
          </Link>
          <div className="mt-[50px]">
            <Image src={avatars} alt="" layout="fixed" width={200} />
            <p className="text-lg font-medium mt-[10px]">
              12,000+ are currently registered and active in Rootix
            </p>
          </div>
        </div>
        <div className="basis-7/12 relative">
          <span>
            <Image src={startTrade} alt=""/>
          </span>
          <Parallax translateY={["-50px", "50px"]}>
            <span>
              <div className="flex flex-col bg-white p-4 shadow-lg absolute left-[100px] bottom-[100px] rounded">
                <Link href="#">
                  <a className="mb-2">
                    <Image src={linkdinIcn} alt=""/>
                  </a>
                </Link>
                <Link href="#">
                  <a className="mb-2">
                    <Image src={instaIcn} alt=""/>
                  </a>
                </Link>
                <Link href="#">
                  <a className="mb-2">
                    <Image src={twitterIcn} alt=""/>
                  </a>
                </Link>
                <Link href="#">
                  <a>
                    <Image src={youtubeIcn} alt=""/>
                  </a>
                </Link>
              </div>
            </span>
          </Parallax>
          <div className="hidden xl:block">
            <Parallax translateX={["-50px", "50px"]}>
              <span>
                <div className="flex justify-between bg-white w-[400px] shadow-xl p-5 rounded items-center justify-center absolute right-[50px] bottom-0">
                  <Image src={rootix} alt=""/>
                  <div className="flex flex-col">
                    <p className="font-semibold text-2xl dark:text-black">Follow Rootix</p>
                    <p className="text-grey-dark dark:text-grey-dark">Cryptocurrency Exchange</p>
                  </div>
                  <a
                    className="cursor-pointer px-5 py-3 rounded-md text-sm h-[40px] w-[80px]
                    text-[#108975] bg-[#D5FAF4] border-none rounded w-full text-center transition
                    duration-300 hover:bg-[#108975] hover:text-white flex justify-center items-center"
                  >
                    Follow
                  </a>
                </div>
              </span>
            </Parallax>
          </div>
          <div className="block xl:hidden">
          <span>
                <div className="flex justify-between bg-white w-[400px] shadow-xl p-5 rounded items-center justify-center absolute right-[10px] bottom-[-40px]">
                  <Image src={rootix} alt=""/>
                  <div className="flex flex-col">
                    <p className="font-semibold text-2xl dark:text-black">Follow Rootix</p>
                    <p className="text-grey-dark dark:text-grey-dark">Cryptocurrency Exchange</p>
                  </div>
                  <a
                    className="cursor-pointer px-5 py-3 rounded-md text-sm h-[40px] w-[80px]
                    text-[#108975] bg-[#D5FAF4] border-none rounded w-full text-center transition
                    duration-300 hover:bg-[#108975] hover:text-white flex justify-center items-center"
                  >
                    Follow
                  </a>
                </div>
              </span>
          </div>
        </div>
      </div>
    </div>
  );
}

export default StartTrade;
