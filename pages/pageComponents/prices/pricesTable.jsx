import React, { useState, useEffect } from "react";
import PricesTabelFilters from "./pricesTabelFilters";
import Table from "../../../components/common/table";
import InfiniteScroll from "react-infinite-scroll-component";
import { RefreshIcon } from "@heroicons/react/solid";
import axios from "axios";

function PricesTable({ onSetTopGainers, phrase }) {
  const [coins, setCoins] = useState([]);
  const [notFound, setNotFound] = useState(false);
  const [lastPage, setLastPage] = useState(0);
  const [hasMore, setHasMore] = useState(true);
  const [page, setPage] = useState(2);
  const [coinsFilter, setCoinsFilter] = useState("all");
  const [searchPhrase, setSearchPhrase] = useState("");
  const [timeFrame, setTimeFrame] = useState("_1h");
  const apiUrl = process.env.NEXT_PUBLIC_API_URL;

  const getInitialCoinsData = async () => {
    const coinsRes = await axios.get(`${apiUrl}/market`, {
      params: {
        filter: "all",
        page: 1,
      },
    });
    const initialCoins = await coinsRes?.data?.data?.currencies;
    const topGainers = await coinsRes?.data?.data?.most_value;
    if (initialCoins?.data?.length > 0) {
      setNotFound(false);
      setCoins(initialCoins.data);
      setLastPage(initialCoins.last_page);
      onSetTopGainers(topGainers);
    } else setNotFound(true);
  };

  const getSearchedCoins = async (phrase) => {
    setSearchPhrase(phrase);
    setPage(2);
    setCoins([]);
    const searchedCoinsRes = await axios.get(`${apiUrl}/market`, {
      params: {
        search: phrase,
        page: 1,
      },
    });
    const searchedCoins = await searchedCoinsRes?.data?.data?.currencies;
    if (searchedCoins?.data?.length <= 0) {
      setNotFound(true);
    }
    if (searchedCoins?.data?.length > 0) {
      setNotFound(false);
      setCoins(searchedCoins?.data);
      setLastPage(searchedCoins.last_page);
    }
    if (searchedCoins?.data?.length <= 9) {
      setHasMore(false);
    }
  };
  useEffect(() => {
    if (phrase) {
      getSearchedCoins(phrase);
      setHasMore(true);
    } else {
      getInitialCoinsData();
      setHasMore(true);
    }
  }, [phrase]);

  const getFilteredCoins = async (filter) => {
    setCoinsFilter(filter);
    setPage(2);
    setCoins([]);
    const filteredCoinsRes = await axios.get(`${apiUrl}/market`, {
      params: {
        filter: filter,
        page: 1,
      },
    });
    const filteredCoins = await filteredCoinsRes?.data?.data?.currencies;
    setCoins(filteredCoins.data);
    setLastPage(filteredCoins.last_page);
  };

  const getMoreCoins = async () => {
    const res = await axios.get(`${apiUrl}/market`, {
      params: {
        filter: coinsFilter,
        page: page,
      },
    });
    const newCoins = await res.data.data.currencies.data;
    const newCoinsArr = res.status === 200 ? Object.values(newCoins) : [];
    setCoins([...coins, ...newCoinsArr]);
    if (page < lastPage) {
      setPage(page + 1);
    } else {
      setHasMore(false);
    }
  };
  const getMoreSearchedCoins = async () => {
    const res = await axios.get(`${apiUrl}/market`, {
      params: {
        search: searchPhrase,
        page: page,
      },
    });
    const newCoins = await res.data.data.currencies.data;
    const newCoinsArr = res.status === 200 ? Object.values(newCoins) : [];
    setCoins([...coins, ...newCoinsArr]);

    if (page < lastPage) {
      setPage(page + 1);
    } else {
      setHasMore(false);
    }
  };
  console.log("x", hasMore);
  return (
    <div className="w-full">
      <div className="container mx-auto">
        <div className="flex flex-col items-center pt-[250px]">
          {!notFound ? (
            <>
              <PricesTabelFilters
                onSetCoinsFilter={setCoinsFilter}
                onGetFiltredCoins={getFilteredCoins}
                timeFrame={timeFrame}
                onSetTimeFrame={setTimeFrame}
                filter={coinsFilter}
              />
              <InfiniteScroll
                dataLength={coins?.length}
                next={phrase?.length > 0 ? getMoreSearchedCoins : getMoreCoins}
                hasMore={hasMore}
                loader={
                  <div>
                    <RefreshIcon
                      className="flex animate-spin mt-[30px] mx-auto"
                      width={30}
                      color="#cccccc"
                    />
                  </div>
                }
                endMessage={<></>}
                className="!overflow-y-hidden"
              >
                <Table data={coins} timeFrame={timeFrame} />
              </InfiniteScroll>
            </>
          ) : (
            <p className="text-[#666666]">nothing has been found</p>
          )}
        </div>
      </div>
    </div>
  );
}

export default PricesTable;
