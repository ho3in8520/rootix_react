import React, {useState} from "react";
import { SearchIcon } from "@heroicons/react/solid";
import axios from 'axios';

function PricesHeader({onSetSearchPhrase}) {
  const apiUrl = process.env.NEXT_PUBLIC_API_URL;



  return (
    <div className="w-full bg-navi-blue">
      <div className="container mx-auto pt-[150px] xl:pt-[200px] pb-[200px]">
        <div className="flex flex-col-reverse items-center xl:flex-row xl:justify-between gap-y-5 xl:gap-y-0">
          <div>
            {/* <div className="flex gap-x-[15px] mb-5 text-white">
              <p>24h Market Change :</p>
              <span className="text-green">+ %19.2</span>
            </div> */}

            <h3 className="text-2xl sm:text-3xl lg:text-4xl leading-normal lg:leading-relaxed font-bold text-white">
              Rootix Cryptocurrency Market
            </h3>
          </div>
          <div className="flex justify-between w-[400px]">
            <div className="w-full relative">
              <SearchIcon
                width={30}
                color="white"
                className="absolute top-[13px] left-[10px] cursor-pointer"
                
              />
              <input
                type="text"
                placeholder="Search"
                className="rounded-full pr-5 pl-[50px] py-4 bg-[#2b3053] outline-none text-white w-full"
                onChange={(e) => onSetSearchPhrase(e.currentTarget.value)}
              />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default PricesHeader;
