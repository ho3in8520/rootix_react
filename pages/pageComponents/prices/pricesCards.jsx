import React from "react";
import Image from "next/image";
import { useTheme } from "next-themes";
import _ from "lodash";
import LineChart from "../../../components/common/lineChart";

import dynamic from 'next/dynamic'
const ContentLoader = dynamic(() => import('react-content-loader'), {
  ssr:false
})
const generateRandomData = () => {
  const chartData = Array.from({ length: 10 }, () =>
    Math.floor(Math.random() * 50)
  );
  return chartData;
};

function PricesCards({ topGainers }) {
  
  const {resolvedTheme} = useTheme();
  
  return (
    <div className="w-full absolute bottom-[-180px]">
      <div className="container mx-auto">
        <div className="flex flex-row justify-between overflow-x-auto gap-x-5 px-5 py-5 2xl:px-2 2xl:overflow-hidden">
          {topGainers?.length > 0
            ? topGainers?.map((coin) => (
                <div
                  key={coin.price}
                  className="bg-white shadow-3xl rounded-xl flex flex-col min-w-[300px] py-5 dark:bg-navi-blue-light"
                >
                  <p className="text-[18px] font-medium px-4 my-3">
                    Top Gainer #1
                  </p>
                  <div className="px-4">
                    <Image src={coin?.logo_url} alt="" width={50} height={50} />
                  </div>
                  <p className="my-3 px-4 font-semibold text-[18px]">
                    {coin?.name}
                  </p>
                  <div className="flex px-4 gap-x-2">
                    <p>Price Change: </p>
                    <p
                      className={`${
                        coin?._1d > 0
                          ? "text-green"
                          : coin?._1d < 0
                          ? "text-red"
                          : "text-[#666666]"
                      }`}
                    >
                      {_.floor(coin._1d, 2)} %
                    </p>
                  </div>

                  <div className="h-[100px]">
                    <LineChart
                      borderWidth={2}
                      data={generateRandomData()}
                      trend={coin._1d > 0 ? "bullish" : "bearish"}
                    />
                  </div>
                </div>
              ))
            : [1, 2, 3, 4].map((item) => (
                <div
                  key={item}
                  className="bg-white dark:bg-navi-blue-light shadow-3xl rounded-xl flex flex-col min-w-[300px] min-h-[320px] py-5"
                >
                  <ContentLoader
                    speed={2}
                    width={320}
                    height={290}
                    viewBox="0 0 320 310"
                    backgroundColor={resolvedTheme === "light" ? "#f3f3f3" : "#040D3E"} 
                    foregroundColor={resolvedTheme === "light" ? "#d9d9d9" : "#111C44"}
                  >
                    <rect x="0" y="35" rx="3" ry="3" width="170" height="12" />
                    <circle cx="40" cy="120" r="35" />
                    <rect x="0" y="190" rx="3" ry="3" width="150" height="15" />
                    <rect x="0" y="240" rx="3" ry="3" width="190" height="9" />
                  </ContentLoader>
                </div>
              ))}
         
        </div>
         
      </div>
    </div>
  );
}


export default PricesCards;


