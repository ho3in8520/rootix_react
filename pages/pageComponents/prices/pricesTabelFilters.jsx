import React from "react";
import Select from "react-select";
import { useTheme } from "next-themes";

function PricesTabelFilters({
  onSetCoinsFilter,
  onGetFiltredCoins,
  filter,
  timeFrame,
  onSetTimeFrame,
}) {
  const doTheFilter = (filter) => {
    onSetCoinsFilter(filter);
    onGetFiltredCoins(filter);
  };
const {resolvedTheme: theme} = useTheme();
  const filters = [
    { name: "All Coins", filter: "all" },
    { name: "Newest", filter: "newest" },
    { name: "Tradeable", filter: "tradeable" },
    { name: "Top Gainers", filter: "" },
    { name: "Top Loosers", filter: "" },
  ];

  const coinTimeFrames = [
    { name: "1h", filter: "_1h" },
    { name: "1d", filter: "_1d" },
    { name: "1W", filter: "_7d" },
    { name: "1M", filter: "_30d" },
    { name: "3M", filter: "_90d" },
  ];

  const selectStyles = {
    control: (provided, state) => ({
      ...provided,
      border: "0px",
      height: "50px",
      boxShadow:
        "rgba(14, 30, 37, 0.12) 0px 2px 4px 0px, rgba(14, 30, 37, 0.32) 0px 2px 16px 0px",
      borderRadius: "10px",
      paddingLeft: "10px",
      background: "transparent",
    }),
    menu: (provided, state) => ({
      ...provided,
      background: theme === "dark" ?  "#2b3053" : "white",
     
    }),
    option: (provided, state) => ({
      ...provided,
      background: "transparent",
      color: theme === "dark" ? "white" : "#666666"
    }),
  };
  return (
    <div className="w-full">
      <div className="hidden 2xl:flex justify-between gap-x-3 ">

        <div className="flex 2xl:bg-grey dark:bg-[#2b3053] rounded-full p-1 text-grey-dark gap-x-1">
          {filters.map((coinFilter) =>
            coinFilter.filter ? (
              <div
              key={coinFilter.name}
                className={`px-[30px] rounded-full py-4 cursor-pointer hover:bg-purple hover:text-white transition dark:text-white ${
                  (filter === coinFilter?.filter) &&
                  "bg-purple text-white dark:bg-navi-blue-light"
                }`}
                onClick={() => doTheFilter(coinFilter?.filter)}
              >
                {coinFilter?.name}
              </div>
            ) : (
              <div key={coinFilter.name} className="px-[30px] py-4 rounded-full cursor-pointer  transition text-[#999999] w-[150px]">
                {coinFilter?.name}
              </div>
            )
          )}
        </div>

        <div className="flex 2xl:bg-grey rounded-full p-1 dark:bg-[#2b3053] gap-x-1">
          {coinTimeFrames?.map((time) => (
            <div 
            key={time.name}
              className={`px-[40px] rounded-full py-4 cursor-pointer hover:bg-purple hover:text-white transition dark:text-white ${
                timeFrame === (time?.filter) &&
                "bg-purple text-white dark:bg-navi-blue-light"
              }`}
              onClick={() => onSetTimeFrame(time?.filter)}
            >
              {time?.name}
            </div>
          ))}
        </div>
      </div>

      <div className="flex justify-between 2xl:hidden gap-x-5">
        <div className="basis-1/2">
          <Select
            className="dark:bg-navi-blue-light rounded-xl"
            options={filters}
            styles={selectStyles}
            defaultValue={filters[0]}
            getOptionLabel={(e) => (
              <div
                className="dark:text-white"
                style={{ display: "flex", alignItems: "center" }}
              >
                <span
                  style={{ marginLeft: 5, color: !e.filter && "#cccccc" }}
                  onClick={() => e.filter &&doTheFilter(e?.filter)}
                >
                  {e.name}
                </span>
              </div>
            )}
          />
        </div>

        <div className="basis-1/2">
          <Select
            className="dark:bg-navi-blue-light rounded-xl"
            options={coinTimeFrames}
            styles={selectStyles}
            defaultValue={coinTimeFrames[0]}
            getOptionLabel={(e) => (
              <div
                className="dark:text-white"
                style={{ display: "flex", alignItems: "center" }}
              >
                <span
                  style={{ marginLeft: 5 }}
                  onClick={() => onSetTimeFrame(e?.filter)}
                >
                  {e.name}
                </span>
              </div>
            )}
          />
        </div>
      </div>
    </div>
  );
}

export default PricesTabelFilters;
