import Layout from "../components/layouts/layout";
import Banner from "./pageComponents/home/banner";
import Swap from "./pageComponents/home/swap";
import Coins from "./pageComponents/home/coins";
import Partners from "./pageComponents/home/partners";
import Features from "./pageComponents/home/features";
import StartTrade from "./pageComponents/home/startTrade";
import Faq from "./pageComponents/home/Faq";
import Comments from "./pageComponents/home/comments";
import axios from "axios";

export default function Home({ coinsData }) {
  
  return (
    <Layout page="main">
      <Banner />
      <div className="relative pt-[150px] dark:bg-navi-blue">
        <Coins data={coinsData} />
        <Swap data={coinsData}/>
      </div>
      <Partners />
      <Features />
      <StartTrade />
      <Comments />
      <Faq />
    </Layout>
  );
}

export const getServerSideProps = async () => {
  //fetch first page coins data
  const apiUrl = process.env.NEXT_PUBLIC_API_URL;
  const coinsRes = await axios.get(`${apiUrl}`);
  const firstPageCoinsData = await coinsRes?.data;

  return {
    props: {
      coinsData: {
        coins: firstPageCoinsData?.data,
        message: firstPageCoinsData?.message,
        status: coinsRes?.status,
      },
    },
  };
};
