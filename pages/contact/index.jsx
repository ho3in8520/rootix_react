import React from "react";
import Layout from "../../components/layouts/layout";
import ContactSupport from "../pageComponents/contact/contactSupport";
import ContactGoogleMap from "../pageComponents/contact/contactGoogleMap";
import ContactForm from "../pageComponents/contact/contactForm";

const Contact = () => {
  return (
    <Layout page="contact">
      <ContactSupport />
      <div className="w-full mt-[50px]">
        <div className="container mx-auto">
          <div className="flex flex-col lg:flex-row ">
            <div className="basis-1/2 py-[50px]">
                <ContactForm />
            </div>
            <div className="basis-1/2">
                <ContactGoogleMap />
            </div>
          </div>
        </div>
      </div>
    </Layout>
  );
};

export default Contact;
