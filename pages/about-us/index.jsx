import Layout from "../../components/layouts/layout";
import AboutTeam from "../pageComponents/about/aboutTeam";
import AboutBox from "../pageComponents/about/aboutBox";
import AboutJoinUs from "../pageComponents/about/aboutJoinUs";
import AboutService from "../pageComponents/about/aboutService";
import AboutMockups from "../pageComponents/about/aboutMockups";

const AboutUs = () => {
    return (
         <Layout>
            <AboutTeam />
            <div className="relative">
                <AboutBox />
                <AboutJoinUs />
            </div>
            <AboutService />
            <AboutMockups />
         </Layout>
    );
}
 
export default AboutUs;