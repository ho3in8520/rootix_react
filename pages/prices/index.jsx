import React, { useState } from "react";
import Layout from "../../components/layouts/layout";
import PricesCards from "../pageComponents/prices/pricesCards";
import PricesHeader from "../pageComponents/prices/pricesHeader";
import PricesTable from "../pageComponents/prices/pricesTable";

export default function Prices() {
  
  const [topGainers, setTopGainers] = useState([]);
  const [searchPhrase, setSearchPhrase] = useState("");

  return (
    <Layout page="prices">
      <div className="relative">
        <PricesHeader onSetSearchPhrase={setSearchPhrase}/>
        <PricesCards topGainers={topGainers} />
      </div>
      <PricesTable onSetTopGainers={setTopGainers} phrase={searchPhrase}/>
    </Layout>
  );
}
